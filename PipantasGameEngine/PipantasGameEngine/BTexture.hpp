#pragma once

#include <cstdint>
#include <string>

#include <gl/glew.h>

struct BTexture final
{
	public:
		BTexture();
		BTexture(GLuint ID_, std::uint32_t Height_, std::uint32_t Width_);
		~BTexture() = default;

		GLuint ID;
		std::uint32_t Height;
		std::uint32_t Width;

		BTexture& operator=(const BTexture& Other);
		operator GLuint() const;
};
