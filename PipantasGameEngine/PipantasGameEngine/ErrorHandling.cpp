#include "ErrorHandling.hpp"
#include "WSDLMessageBox.hpp"
#include "Managers.hpp"
#include "MyStackWalker.hpp"
#include "rapidxml.hpp"

#include <gl\glew.h>
#include <exception>
#include <stdlib.h>

namespace Pipantas
{
	[[noreturn]] void OnTerminate() noexcept
	{
		if (std::exception_ptr Exc = std::current_exception())
		{
			ShowErrorMessageBox();

			try
			{
				std::rethrow_exception(Exc);
			}
			catch (const Pipanikos& e)
			{
				Logger.Log(ELogLevel::Error, "PIPANIKOS: " + std::string(e.what()));
			}
			catch (const std::exception& e)
			{
				Logger.Log(ELogLevel::Error, "STANDARD EXCEPTION: " + std::string(e.what()));
			}
			catch (const rapidxml::parse_error& e)
			{
				Logger.Log(ELogLevel::Error, "RAPIDXML EXCEPTION: " + std::string(e.what()));
			}
			catch (...)
			{
				Logger.Log(ELogLevel::Error, "UNKNOWN EXCEPTION!");
			}

			// For debugging...
			//MyStackWalker StackWalker;
			//StackWalker.ShowCallstack();
			std::_Exit(EXIT_FAILURE);
		}
	}

	void ShowErrorMessageBox()
	{
		WSDLMessageBox ErrorMessagebox("Pipantas Engine", "Pipantas has encountered a fatal error and could not continue execution.\nPlease see the log file for more information.");
		ErrorMessagebox.Show();
	}

	void CheckGLError(std::string Statement)
	{
		GLenum Error = glGetError();
		std::stringstream Stringstream;
		Stringstream << "0x" << std::hex << Error << std::endl << Statement;

		if (Error != GL_NO_ERROR)
		{
			throw(Pipanikos("OpenGL error: " + Stringstream.str(), __FILE__, __LINE__));
			ShowErrorMessageBox();
		}
	}
}