#include "BShader.hpp"
#include "Managers.hpp"
#include "Pipanikos.hpp"
#include "ErrorHandling.hpp"

BShader::BShader()
	: bIsLoaded(false)
{
}

void BShader::Load(std::string Path_, GLenum Type_)
{
	std::string ShaderData = ResourceLoader.GetFileContents(Path_);

	ID = glCreateShader(Type_);
	if (ID != 0)
	{
		const char* RawData = ShaderData.c_str();
		GL_CALL(glShaderSource(ID, 1, &RawData, nullptr));
		GL_CALL(glCompileShader(ID));

		GLint CompilationStatus;
		GL_CALL(glGetShaderiv(ID, GL_COMPILE_STATUS, &CompilationStatus));

		if (CompilationStatus == GL_FALSE)
		{
			std::vector<GLchar> ShaderErrorLog;
			GLint LogLength;

			GL_CALL(glGetShaderiv(ID, GL_INFO_LOG_LENGTH, &LogLength));
			ShaderErrorLog.resize(LogLength);

			GL_CALL(glGetShaderInfoLog(ID, LogLength, nullptr, ShaderErrorLog.data()));

			// Subtract 1 from ShaderErrorLog.end() to remove the null terminator.
			throw(Pipanikos("Shader compilation failed.\n\tPath: " + Path_ + "\n\n\t" + std::string(ShaderErrorLog.begin(), ShaderErrorLog.end() - 1), __FILE__, __LINE__));
		}

		Type = Type_;
		bIsLoaded = true;
		Path = Path_;
	}
	else
	{
		throw(Pipanikos("Failed to create shader from " + Path_, __FILE__, __LINE__));
	}
	
}

void BShader::Delete(const KeyShaderProgram&)
{
	if (bIsLoaded)
	{
		GL_CALL(glDeleteShader(ID));
		bIsLoaded = false;
	}
}

bool BShader::IsLoaded() const
{
	return bIsLoaded;
}

const std::string BShader::GetPath() const
{
	return Path;
}

BShader::operator GLuint() const
{
	return ID;
}
