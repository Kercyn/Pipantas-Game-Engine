#include "SITriggerCollider.hpp"
#include "Managers.hpp"
#include "glm\glm.hpp"
#include "Pipanikos.hpp"

SITriggerCollider::SITriggerCollider(LUA_INTEGER ID)
{
	for (const auto& KVP : CollisionEngine.GetColliders())
	{
		auto CastID = static_cast<std::uint16_t>(ID);
		if (KVP.second->IsTrigger())
		{
			if (KVP.second->GetID() == CastID)
			{
				Collider = static_cast<PTriggerCollider*>(KVP.second);
			}
		}
	}

	if (Collider == nullptr)
	{
		throw(Pipanikos("Unable to find collider " + static_cast<unsigned>(ID), __FILE__, __LINE__));
	}
}

std::string SITriggerCollider::GetAttachedActorID() const
{
	if (auto AttachedActor = Collider->GetAttachedActor())
	{
		return AttachedActor->GetID();
	}
	else
	{
		throw(Pipanikos("Collider " + Collider->GetName() + " has no attached actor.", __FILE__, __LINE__));
	}
}

std::string SITriggerCollider::GetProperty(std::string ID) const
{
	return Collider->GetProperty(ID);
}

bool SITriggerCollider::IsActive() const
{
	return (Collider->IsEnabledSelf() && Collider->IsEnabledParent());
}

bool SITriggerCollider::IsEnabled() const
{
	return Collider->IsEnabledSelf();
}

bool SITriggerCollider::HasAttachedActor() const
{
	return (Collider->GetAttachedActor() != nullptr);
}

std::string SITriggerCollider::GetName() const
{
	return Collider->GetName();
}

std::tuple<LUA_NUMBER, LUA_NUMBER> SITriggerCollider::GetSize() const
{
	auto Size = Collider->GetShape()->GetSize();
	return std::make_tuple(Size.x, Size.y);
}

std::tuple<LUA_NUMBER, LUA_NUMBER> SITriggerCollider::GetOffset() const
{
	auto Offset = Collider->GetOffset();
	return std::tuple<LUA_NUMBER, LUA_NUMBER>(Offset.x, Offset.y);
}

void SITriggerCollider::Enable()
{
	Collider->Enable();
}

void SITriggerCollider::Disable()
{
	Collider->Disable();
}

void SITriggerCollider::SetOffset(LUA_NUMBER x, LUA_NUMBER y)
{
	Collider->SetOffset(glm::vec2(x, y));
}
