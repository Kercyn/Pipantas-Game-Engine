#pragma once

/*! \file */

#include <queue>
#include <cstdint>
#include <vector>
#include <functional>
#include <map>

#include "IManager.hpp"
#include "PEndPoint.hpp"
#include "PCollider.hpp"
#include "PSAP.hpp"
#include "SAABB.hpp"
#include "AccessKeys.hpp"

class MCollisionEngine;

/*! Implements a broadphase collision detection. Responsible for finding objects that might be colliding.
*/
class MPairGenerator final : public IManager
{
	public:
		MPairGenerator() = default;
		~MPairGenerator() override = default;

		void Initialize(const KeyGameCoordinator&, const std::vector<std::string>&) override;
		void Shutdown(const KeyGameCoordinator&) override;
		void Reset();

		//! Calculate the number of total SAP areas, find their boundaries and assign their appropriate colliders and endpoints.
		void UpdateSAPAreas(const KeySceneSwitcher&);

		//! Populates the CollidingPairs attribute with potentially colliding pairs.
		void GeneratePairs(const KeyCollisionEngine&);

		const std::vector<PSAP>& GetSAPs() const;
		const std::vector<std::pair<std::uint16_t, std::uint16_t>>& GetCollidingPairs() const;

	private:
		//! The world's SAP areas.
		std::vector<PSAP> SAPs;

		/*! Pairs of collider IDs that are potentially colliding.
		Even though colliding pairs are unique, they're stored in a std::vector (instead of perhaps a container
		that actively enforces uniqueness) as there is no way to insert duplicates.
		PSAP::GeneratePairs is responsible for the pair generation.
		*/
		std::vector<std::pair<std::uint16_t, std::uint16_t>> CollidingPairs;
};
