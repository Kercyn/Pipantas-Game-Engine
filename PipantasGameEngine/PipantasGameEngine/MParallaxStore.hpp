#pragma once

#include "IManager.hpp"
#include "CParallax.hpp"
#include "AccessKeys.hpp"

#include <map>
#include <string>

class MParallaxStore final : public IManager
{
	public:
		MParallaxStore() = default;
		~MParallaxStore() override = default;

		void Initialize(const KeyGameCoordinator&, const std::vector<std::string>&) override;
		void Shutdown(const KeyGameCoordinator&) override;
		void Reset();

		const std::map<std::string, CParallax>& GetCurrentGroup() const;
		CParallax& GetParallax(std::string Key);
		std::map<std::string, CParallax>& GetCurrentGroup(const KeyParallaxLogic&);
		
		//! Updates the current scene parallaxes' information.
		void UpdateParallaxesForScene();

	private:
		std::map<std::string, CParallax> CurrentGroup;
};
