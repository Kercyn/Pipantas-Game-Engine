#include "Pipanikos.hpp"

Pipanikos::Pipanikos(const char* const Message_, std::string File, int Line) throw()
{
	Message = std::string(Message_) + "\nFile: " + File + "\nLine: " + std::to_string(Line);
}

Pipanikos::Pipanikos(const std::string& Message_, std::string File, int Line) throw()
{
	Message = Message_ + "\nFile: " + File + "\nLine: " + std::to_string(Line);
}

const char* Pipanikos::what() const
{
	return Message.c_str();
}
