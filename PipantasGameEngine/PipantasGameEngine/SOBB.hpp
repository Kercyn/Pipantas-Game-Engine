#pragma once

/*! \file */

#include "SShape.hpp"
#include "PLine.hpp"
#include "glm\glm.hpp"

class SOBB final : public SShape
{
	public:
		SOBB();
		~SOBB() override = default;

		std::unique_ptr<SShape> Clone() const override;
		Line ProjectInto(const glm::vec2& Axis) const override;
		bool IsPointInside(const glm::vec2& Point) const override;
		glm::mat4 GetModelMatrix() const override;
		glm::vec2 GetSize() const override;

		glm::vec2 GetSupportPoint(const glm::vec2& Direction) const;

		void SetSize(float Height, float Width);
		float GetHalfHeight() const;
		float GetHalfWidth() const;

		void Rotate(float Radians);
		void SetRotation(float Radians);

	private:
		float HalfHeight;
		float HalfWidth;

		void UpdateRotationMat();
		void UpdateVerticesAndNormals() override;
};
