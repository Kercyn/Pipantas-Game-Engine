#include "SShape.hpp"
#include "glm\gtx\transform.hpp"

SShape::SShape()
	: Rotation(0.0f), Position(glm::vec2()), Scale(glm::vec2(1.0f)),
	TranslationMat(glm::mat4(1.0f)), ScaleMat(glm::mat2(1.0f)), RotationMat(glm::mat2(1.0f))
{
}

void SShape::SetPosition(const glm::vec2& NewPosition)
{
	Position = NewPosition;
	UpdateTranslationMat();
}

void SShape::SetPosition(float NewX, float NewY)
{
	Position.x = NewX;
	Position.y = NewY;
	UpdateTranslationMat();
}

void SShape::Translate(float OffsetX, float OffsetY)
{
	Position.x += OffsetX;
	Position.y += OffsetY;
	UpdateTranslationMat();
}

void SShape::Translate(const glm::vec2& Offset)
{
	Position += Offset;
	UpdateTranslationMat();
}

void SShape::SetScale(glm::vec2 NewScale)
{
	Scale = NewScale;
	UpdateScaleMat();
}

void SShape::SetScale(float x, float y)
{
	SetScale(glm::vec2(x, y));
}

void SShape::ChangeScale(float NewX, float NewY)
{
	Scale.x += NewX;
	Scale.y += NewY;
	UpdateScaleMat();
}

const std::vector<glm::vec2>& SShape::GetVertices() const
{
	return Vertices;
}

const std::vector<glm::vec2>& SShape::GetNormals() const
{
	return Normals;
}

const glm::vec2& SShape::GetPosition() const
{
	return Position;
}

glm::vec2 const& SShape::GetScale() const
{
	return Scale;
}

EShapeType SShape::GetType() const
{
	return Type;
}

const glm::mat2& SShape::GetScaleMat() const
{
	return ScaleMat;
}

float SShape::GetRotation() const
{
	return Rotation;
}

const glm::mat2& SShape::GetRotationMat() const
{
	return RotationMat;
}

void SShape::UpdateTranslationMat()
{
	TranslationMat = glm::translate(glm::mat4(1.0f), glm::vec3(Position.x, Position.y, 0.0f));
}

void SShape::UpdateScaleMat()
{
	ScaleMat = glm::mat2(glm::scale(glm::vec3(Scale.x, Scale.y, 1.0f)));
}
