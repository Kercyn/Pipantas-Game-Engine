#include "SIActor.hpp"
#include "Managers.hpp"
#include "Pipanikos.hpp"

SIActor::SIActor(std::string ID)
{
	Actor = ActorHandler.GetActor(ID);
}

std::tuple<LUA_NUMBER, LUA_NUMBER> SIActor::GetPosition() const
{
	const auto& ActorPos = Actor->GetBody()->GetPosition();

	return std::make_tuple(ActorPos.x, ActorPos.y);
}

std::tuple<LUA_NUMBER, LUA_NUMBER> SIActor::GetOrientation() const
{
	const auto& Orientation = Actor->GetOrientation();

	return std::make_tuple(static_cast<LUA_NUMBER>(Orientation.x), static_cast<LUA_NUMBER>(Orientation.y));
}

std::string SIActor::GetKey() const
{
	return Actor->GetKey();
}

std::string SIActor::GetID() const
{
	return Actor->GetID();
}

std::string SIActor::GetProperty(std::string PropKey) const
{
	return Actor->GetProperty(PropKey);
}

bool SIActor::IsEnabled() const
{
	return Actor->IsEnabled();
}

bool SIActor::HasCollider(std::string Name) const
{
	return (Actor->GetCollider(Name) != nullptr);
}

LUA_NUMBER SIActor::GetColliderID(std::string Name) const
{
	if (auto Collider = Actor->GetCollider(Name))
	{
		return static_cast<LUA_INTEGER>(Collider->GetID());
	}
	else
	{
		throw(Pipanikos("Could not find collider " + Name, __FILE__, __LINE__));
	}
}

void SIActor::Translate(LUA_NUMBER x, LUA_NUMBER y)
{
	float dt = GameCoordinator.GetDeltaTime();
	Actor->Translate(x * dt, y * dt);
}

void SIActor::Rotate(LUA_NUMBER Angle)
{
	float dt = GameCoordinator.GetDeltaTime();
	Actor->Rotate(Angle * dt);
}

void SIActor::SetPosition(LUA_NUMBER NewX, LUA_NUMBER NewY)
{
	Actor->SetPosition(NewX, NewY);
}

void SIActor::SetRotation(LUA_NUMBER NewRotation)
{
	Actor->SetRotation(NewRotation);
}

void SIActor::SetOrientation(LUA_NUMBER OrientX, LUA_NUMBER OrientY)
{
	Actor->SetOrientation(OrientX, OrientY);
}

void SIActor::SetProperty(std::string PropKey, std::string Value)
{
	Actor->SetProperty(PropKey, Value);
}

void SIActor::Enable()
{
	Actor->Enable();
}

void SIActor::Disable()
{
	Actor->Disable();
}
