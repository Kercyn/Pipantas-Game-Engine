#pragma once

/*! \file */

#include "luaconf.h"
#include "Selene.h"
#include "CCamera.hpp"

#include <tuple>

class SICamera final
{
	public:
		SICamera();
		~SICamera() = default;
	
		std::tuple<LUA_NUMBER, LUA_NUMBER> GetPosition() const;
		float GetZoom() const;

		void Translate(LUA_NUMBER x, LUA_NUMBER y) const;
		void SetPosition(LUA_NUMBER x, LUA_NUMBER y) const;
		void SetZoom(LUA_NUMBER NewZoom) const;

	private:
		CCamera& Camera;
		sel::State& State;
};
