#include "SOBB.hpp"
#include "glm\glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm\gtx\projection.hpp"
#include "Utils.hpp"

#include <array>
#include <algorithm>
#include <vector>
#include <limits>

SOBB::SOBB()
	: HalfHeight(0.0f), HalfWidth(0.0f)
{
	Type = EShapeType::OBB;
}

std::unique_ptr<SShape> SOBB::Clone() const
{
	return std::make_unique<SOBB>(*this);
}

Line SOBB::ProjectInto(const glm::vec2& Axis) const
{
	auto First = GetSupportPoint(Axis);
	First = glm::proj(First, Axis);

	auto Second = GetSupportPoint(-Axis);
	Second = glm::proj(Second, -Axis);

	return Line(First, Second);
}

bool SOBB::IsPointInside(const glm::vec2& Point) const
{
	const glm::vec2 Top = GetSupportPoint(glm::vec2(0.0f, 1.0f));
	const glm::vec2 Bottom = GetSupportPoint(glm::vec2(0.0f, -1.0f));
	const glm::vec2 Left = GetSupportPoint(glm::vec2(-1.0f, 0.0f));
	const glm::vec2 Right = GetSupportPoint(glm::vec2(1.0f, 0.0f));

	return ((Point.x >= Left.x) && (Point.x <= Right.x)
		&& (Point.y >= Bottom.y) && (Point.y <= Top.y));
}

glm::mat4 SOBB::GetModelMatrix() const
{
	return TranslationMat * glm::mat4(RotationMat) * glm::mat4(ScaleMat);
}

void SOBB::Rotate(float Radians)
{
	Rotation += Radians;

	while (Rotation >= (2 * Utils::PI))
	{
		Rotation -= (2 * Utils::PI);
	}

	UpdateRotationMat();
}

void SOBB::SetRotation(float Radians)
{
	Rotation = Radians;

	while (Rotation >= (2 * Utils::PI))
	{
		Rotation -= (2 * Utils::PI);
	}

	UpdateRotationMat();
}

glm::vec2 SOBB::GetSupportPoint(const glm::vec2& Direction) const
{
	const auto& Vertices = GetVertices();

	float BestProjection = -std::numeric_limits<float>::max();
	glm::vec2 BestVertex;

	for (glm::vec2 Vertex : Vertices)
	{
		Vertex = Vertex * RotationMat;
		Vertex = Vertex * ScaleMat;

		float Projection = glm::dot(Vertex, Direction);

		if (Projection > BestProjection)
		{
			BestProjection = Projection;
			BestVertex = Vertex;
		}
	}

	return BestVertex + Position;
}

void SOBB::SetSize(float Height, float Width)
{
	HalfHeight = Height / 2.0f;
	HalfWidth = Width / 2.0f;

	UpdateVerticesAndNormals();
}

glm::vec2 SOBB::GetSize() const
{
	return glm::vec2(HalfWidth * 2, HalfHeight * 2);
}

float SOBB::GetHalfHeight() const
{
	return HalfHeight;
}

float SOBB::GetHalfWidth() const
{
	return HalfWidth;
}

void SOBB::UpdateRotationMat()
{
	RotationMat = glm::mat2(glm::rotate(glm::mat4(1.0f), Rotation, glm::vec3(0.0f, 0.0f, 1.0f)));
}

void SOBB::UpdateVerticesAndNormals()
{
	Vertices.clear();
	Normals.clear();

	Vertices.emplace_back(glm::vec2(-HalfWidth, -HalfHeight));
	Vertices.emplace_back(glm::vec2(-HalfWidth, HalfHeight));
	Vertices.emplace_back(glm::vec2(HalfWidth, HalfHeight));
	Vertices.emplace_back(glm::vec2(HalfWidth, -HalfHeight));

	Normals.emplace_back(glm::vec2(0.0f, -1.0f));
	Normals.emplace_back(glm::vec2(-1.0f, 0.0f));
	Normals.emplace_back(glm::vec2(0.0f, 1.0f));
	Normals.emplace_back(glm::vec2(1.0f, 0.0f));
}
