#pragma once

/*! \file */

#include <string>
#include <gl/glew.h>

#include "AccessKeys.hpp"

class BShader final
{
	public:
		BShader();
		~BShader() = default;

		//! Loads and compiles the shader at Path.
		void Load(std::string Path, GLenum Type);

		void Delete(const KeyShaderProgram&);

		bool IsLoaded() const;

		const std::string GetPath() const;

		operator GLuint() const;

	private:
		GLuint ID;
		GLenum Type;
		bool bIsLoaded;

		//! The path of the shader's source file. Needed for debugging/logging purposes.
		std::string Path;
};
