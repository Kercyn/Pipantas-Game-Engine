#pragma once

#include "SDL.h"
#include "Utils.hpp"
#include "glm\glm.hpp"
#include "SAABB.hpp"

class CCamera final
{
	public:
		CCamera();
		~CCamera() = default;

		const glm::vec3& GetPosition() const;
		float GetFoV() const;
		float GetNearPlane() const;
		float GetFarPlane() const;
		glm::mat4 GetOrientation() const;

		void SetPosition(const glm::vec2& Position_);
		void SetNearFarPlanes(float Near, float Far);
		void SetZoom(float NewZoom);
		void Translate(const glm::vec2& Offset);
		void Translate(float x, float y);
		//void OffsetOrientation(float Up, float Right);

		//void LookAt(const glm::vec3& Position);

		// Direction unit vectors
		glm::vec3 Forward() const;
		glm::vec3 Right() const;
		glm::vec3 Up() const;

		//! Returns the projection transformation matrix;
		glm::mat4 Projection() const;

		//! Returns the translation and rotation matrix.
		glm::mat4 View() const;

		//! Returns the complete camera matrix to be used in the vertex shader.
		glm::mat4 Matrix() const;

	private:
		//! In degrees. Must be less than 90 to avoid gimbal lock.
		//static const float MaxVerticalAngle;

		glm::vec3 Position;

		float HorizontalAngle;
		float VerticalAngle;

		float FieldOfView;
		float NearPlane;
		float FarPlane;

		//void NormalizeAngles();
};