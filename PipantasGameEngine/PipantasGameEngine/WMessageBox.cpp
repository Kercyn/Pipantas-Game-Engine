#include "WMessageBox.hpp"
#include "SDL.h"

#include <vector>

std::vector<MsgboxButton> WMessageBox::MsgboxButtons = {
	MsgboxButton(MsgboxButtonFlag::OK, "OK", SDL_SCANCODE_RETURN),
	MsgboxButton(MsgboxButtonFlag::Cancel, "Cancel", SDL_SCANCODE_ESCAPE),
	MsgboxButton(MsgboxButtonFlag::Yes, "Yes", SDL_SCANCODE_RETURN),
	MsgboxButton(MsgboxButtonFlag::No, "No", SDL_SCANCODE_ESCAPE)
};

std::map<EMsgboxType, uint8_t> WMessageBox::ButtonDescription = {
	{ EMsgboxType::OK_Cancel, MsgboxButtonFlag::OK | MsgboxButtonFlag::Cancel },
	{ EMsgboxType::Yes_No, MsgboxButtonFlag::Yes | MsgboxButtonFlag::No },
	{ EMsgboxType::Info, MsgboxButtonFlag::OK },
	{ EMsgboxType::Warning, MsgboxButtonFlag::OK },
	{ EMsgboxType::Error, MsgboxButtonFlag::OK }
};

WMessageBox::WMessageBox(std::string Title_, std::string Message_, EMsgboxType MsgboxType_) : Title(Title_), Message(Message_), MsgboxType(MsgboxType_)
{
}