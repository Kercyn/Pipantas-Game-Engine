#include "BTTFFontData.hpp"
#include "SDL_ttf.h"

BTTFFontData::BTTFFontData()
	: Color(SDL_Color()), bKerning(false), Attributes(TTF_STYLE_NORMAL), Key()
{}
