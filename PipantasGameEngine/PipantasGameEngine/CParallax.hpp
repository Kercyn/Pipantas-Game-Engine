#pragma once

#include <cstdint>
#include <string>

#include "glm\glm.hpp"

enum class EParallaxType
{
	Bi_X,
	Bi_Y,
	Multi,
	Fixed
};

struct CParallax final
{
	public:
		CParallax() = default;
		~CParallax() = default;

		EParallaxType Type;
		float Speed;
		glm::vec2 Offset;
		glm::vec2 Size;
		bool bIsEnabled;
		bool bRenderFront;
};
