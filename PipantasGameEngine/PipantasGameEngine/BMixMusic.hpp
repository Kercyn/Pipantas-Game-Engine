#pragma once

#include <memory>
#include <vector>
#include "SDL_mixer.h"
#include "physfs.h"

class MResourceLoader;

class BMixMusic final
{
	public:
		using MusicPtr = std::unique_ptr<Mix_Music, decltype(&Mix_FreeMusic)>;
		using RWopsPtr = std::unique_ptr<SDL_RWops, decltype(&SDL_FreeRW)>;

		BMixMusic();
		BMixMusic(MusicPtr Music_, RWopsPtr RWops_);
		BMixMusic(BMixMusic&& Other);
		BMixMusic(const BMixMusic& Other) = delete;
		~BMixMusic() = default;

		BMixMusic& operator=(const BMixMusic& Other) = delete;
		BMixMusic& operator=(BMixMusic&& Other);

		operator Mix_Music *();

		friend MResourceLoader;

	private:
		MusicPtr Music;
		RWopsPtr RWops;
		std::vector<PHYSFS_sint64> MusicData;
};
