#pragma once

/*! \file */
#include <string>
#include "Managers.hpp"
#include "Pipanikos.hpp"

/*! \brief A struct representing a value returned by the File Parser.
*/
class CParserValue final
{
	public:

		CParserValue(std::string RawValue_) : RawValue(RawValue_) {}
		~CParserValue() {}

		template<typename T>
		operator T() const
		{
			std::stringstream StrStream(RawValue);
			T ConvertedValue;

			if (StrStream >> ConvertedValue)
			{
				return ConvertedValue;
			}
			else
			{
				throw(Pipanikos("Failed to convert " + RawValue, __FILE__, __LINE__));
			}
		}

	private:
		std::string RawValue;
};