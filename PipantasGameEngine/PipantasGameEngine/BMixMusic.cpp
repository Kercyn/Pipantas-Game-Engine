#include "BMixMusic.hpp"

BMixMusic::BMixMusic()
	: Music(nullptr, nullptr), RWops(nullptr, nullptr)
{}

BMixMusic::BMixMusic(MusicPtr Music_, RWopsPtr RWops_)
	: Music(nullptr, nullptr), RWops(nullptr, nullptr)
{
	Music = std::move(Music_);
	RWops = std::move(RWops_);
}

BMixMusic::BMixMusic(BMixMusic&& Other)
	: Music(nullptr, nullptr), RWops(nullptr, nullptr)
{
	if (this != &Other)
	{
		Music = std::move(Other.Music);
		RWops = std::move(Other.RWops);
		MusicData = Other.MusicData;
	}
}

BMixMusic& BMixMusic::operator=(BMixMusic&& Other)
{
	if (this != &Other)
	{
		Music = std::move(Other.Music);
		RWops = std::move(Other.RWops);
		MusicData = Other.MusicData;
	}

	return *this;
}

BMixMusic::operator Mix_Music *()
{
	return Music.get();
}
