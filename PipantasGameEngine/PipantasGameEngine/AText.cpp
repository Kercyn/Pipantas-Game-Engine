#include "AText.hpp"
#include "Managers.hpp"
#include "SDL_ttf.h"
#include "Pipanikos.hpp"

const std::string TextMapProperties::COLOR = "Color";
const std::string TextMapProperties::FONT = "Font";
const std::string TextMapProperties::RENDERING = "Rendering";
const std::string TextMapProperties::SIZE = "Size";
const std::string TextMapProperties::STYLE = "Style";
const std::string TextMapProperties::TEXT = "Text";

AText::AText()
{
	Body = std::make_unique<SOBB>();
}

std::string AText::GetText() const
{
	return Text;
}

void AText::SetText(std::string NewText)
{
	if (Text != NewText)
	{
		Text = NewText;
		SetProperty(TextProperties.TEXT, NewText);
		UpdateTexture();
	}
}

void AText::UpdateTexture()
{
	using SurfacePtr = std::unique_ptr<SDL_Surface, decltype(&SDL_FreeSurface)>;

	auto& Font = ResourceStore.RequestFont(FontID);
	const auto& FontData = ResourceStore.RequestFontData(ID);

	SurfacePtr Surface(nullptr, nullptr);

	if (FontData.Rendering == EFontRendering::Solid)
	{
		Surface = SurfacePtr(TTF_RenderUTF8_Solid(Font, Text.c_str(), FontData.Color), SDL_FreeSurface);
	}
	else if (FontData.Rendering == EFontRendering::Blended)
	{
		Surface = SurfacePtr(TTF_RenderUTF8_Blended_Wrapped(Font, Text.c_str(), FontData.Color, WrapLength), SDL_FreeSurface);
	}

	if (Surface == nullptr)
	{
		throw(Pipanikos("Created surface was null. ID: " + ID + " Text: " + Text + " Error: " + TTF_GetError(), __FILE__, __LINE__));
	}
	else
	{
		ResourceLoader.CreateTextureFromSurface(ID, Surface.get());
		
		SOBB* OBB = static_cast<SOBB*>(Body.get());
		OBB->SetSize(Surface->h, Surface->w);
	}
}

void AText::UpdateInformation(KeyActorHandler, const MapElements::Object& TextSpawnerObj)
{
	const auto& CurrentMap = SceneSwitcher.GetCurrentMap();

	auto A_ID = TextSpawnerObj.Properties.GetProperty(AActor::ActorProperties.A_ID);
	if (A_ID)
	{
		ID = *A_ID;
	}
	else
	{
		throw(Pipanikos("A_ID not found for " + TextSpawnerObj.Name, __FILE__, __LINE__));
	}

	auto KeyProp = TextSpawnerObj.Properties.GetProperty(AActor::ActorProperties.KEY);
	if (KeyProp)
	{
		Key = *KeyProp;
	}
	else
	{
		throw(Pipanikos("No key attribute found for " + TextSpawnerObj.Name, __FILE__, __LINE__));
	}

	auto FontIDProp = TextSpawnerObj.Properties.GetProperty(AText::TextProperties.FONT);
	if (FontIDProp)
	{
		auto FontSizeProp = TextSpawnerObj.Properties.GetProperty(AText::TextProperties.SIZE);
		if (FontSizeProp)
		{
			FontID = *FontIDProp + "_" + *FontSizeProp;
		}
		else
		{
			throw(Pipanikos("No size attribute not found for " + TextSpawnerObj.Name, __FILE__, __LINE__));
		}
	}
	else
	{
		throw(Pipanikos("No font attribute not found for " + TextSpawnerObj.Name, __FILE__, __LINE__));
	}

	auto TextProp = TextSpawnerObj.Properties.GetProperty(AText::TextProperties.TEXT);
	if (TextProp)
	{
		// We don't use ChangeText here because scene resources have not been loaded yet, so we
		// can't change the texture now. Text textures are updated by the resource manager afterwards.
		Text = *TextProp;
	}
	else
	{
		throw(Pipanikos("No text attribute found for " + TextSpawnerObj.Name, __FILE__, __LINE__));
	}

	WrapLength = TextSpawnerObj.Width;

	glm::vec2 Position;

	// We're adding half WrapLength because in Tiled an object's position is the vertex of the object that is closest to the origin.
	Position.x = TextSpawnerObj.Position.x + WrapLength / 2.0f;
	// Need to invert the height because OpenGL
	Position.y = (CurrentMap->GetHeight() * CurrentMap->GetTileSize().y) - TextSpawnerObj.Position.y;

	Body->SetPosition(Position);

	CopyProperties(TextSpawnerObj);
}
