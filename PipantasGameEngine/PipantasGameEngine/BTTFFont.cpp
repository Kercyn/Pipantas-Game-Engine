#include "BTTFFont.hpp"

BTTFFont::BTTFFont()
	: Key(), Font(nullptr, nullptr), RWops(nullptr, nullptr), Size(14)
{}

BTTFFont::BTTFFont(FontPtr Font_, RWopsPtr RWops_, std::string Key_, int Size_)
	: Key(Key_), Font(nullptr, nullptr), RWops(nullptr, nullptr), Size(Size_)
{
	Font = std::move(Font_);
	RWops = std::move(RWops_);
}

BTTFFont::BTTFFont(BTTFFont&& Other)
	: Font(nullptr, nullptr), RWops(nullptr, nullptr)
{
	if (this != &Other)
	{
		Key = Other.Key;
		Font = std::move(Other.Font);
		RWops = std::move(Other.RWops);
		Size = Other.Size;
		FontData = Other.FontData;
	}
}

BTTFFont& BTTFFont::operator=(BTTFFont&& Other)
{
	if (this != &Other)
	{
		Key = Other.Key;
		Font = std::move(Other.Font);
		RWops = std::move(Other.RWops);
		Size = Other.Size;
		FontData = Other.FontData;
	}

	return *this;
}

BTTFFont::operator TTF_Font*()
{
	return Font.get();
}
