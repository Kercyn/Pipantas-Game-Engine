#pragma once

/*! \file */

#include "SShape.hpp"
#include "PLine.hpp"
#include "glm\glm.hpp"

//! Axis-aligned bounding box class.
class SAABB final : public SShape
{
	public:
		SAABB();
		~SAABB() override = default;
		
		std::unique_ptr<SShape> Clone() const override;
		Line ProjectInto(const glm::vec2& Axis) const override;
		bool IsPointInside(const glm::vec2& Point) const override;
		glm::mat4 GetModelMatrix() const override;
		glm::vec2 GetSize() const override;

		glm::vec2 GetSupportPoint(const glm::vec2& Direction) const;

		void SetBoundaries(const glm::vec2& Min_, const glm::vec2& Max_);
		void SetMin(float x, float y);
		void SetMax(float x, float y);
		void SetSize(glm::vec2 NewSize);

		const glm::vec2& GetMin() const;
		const glm::vec2& GetMax() const;

	private:
		glm::vec2 Min;
		glm::vec2 Max;

		void UpdateVerticesAndNormals() override;
};
