#pragma once

#include <string>
#include <cstdint>

#include "AActor.hpp"
#include "MapElements.hpp"
#include "SDL_stdinc.h"

class MResourceLoader;

struct TextMapProperties final
{
	static const std::string COLOR;
	static const std::string FONT;
	static const std::string RENDERING;
	static const std::string SIZE;
	static const std::string STYLE;
	static const std::string TEXT;
};

class AText final : public AActor
{
	public:
		AText();
		~AText() override = default;

		std::string GetText() const;

		void SetText(std::string NewText);

		void UpdateInformation(KeyActorHandler, const MapElements::Object& TextSpawnerObj) override;

		static const TextMapProperties TextProperties;

		friend MResourceLoader;

	private:
		std::string Text;
		std::string FontID;
		Uint32 WrapLength;

		//! Updates the actor's texture when its text changes.
		void UpdateTexture();
};
