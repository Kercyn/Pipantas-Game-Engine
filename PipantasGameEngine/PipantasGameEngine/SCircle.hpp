#pragma once

/*! \file */

#include "SShape.hpp"
#include "PLine.hpp"
#include "glm\glm.hpp"

#include <cinttypes>

class SCircle final : public SShape
{
	public:
		SCircle();
		SCircle(glm::vec2 Center_, float Radius_);
		~SCircle() override = default;

		std::unique_ptr<SShape> Clone() const override;
		Line ProjectInto(const glm::vec2& Axis) const override;
		bool IsPointInside(const glm::vec2& Point) const override;
		glm::mat4 GetModelMatrix() const override;
		glm::vec2 GetSize() const override;

		void SetRadius(float NewRadius);
		float GetRadius() const;

		void Rotate(float Radians);
		void SetRotation(float Radians);

		/*! \brief The number of vertices that a circle uses.
		A circle has infinite points at radius distance, but we must pick
		a finite number to represent them.
		*/
		static constexpr std::uint8_t CircleVertices = 32;

	private:
		float Radius;

		//! In radians.
		float Rotation;
		glm::mat2 RotationMat;

		void UpdateRotationMat();
		void UpdateVerticesAndNormals() override;
};
