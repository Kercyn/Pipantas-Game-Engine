#include "Managers.hpp"
#include "ErrorHandling.hpp"

#include <stdlib.h>
#include <vector>
#include <string>
#include <exception>

int main(int argc, char* argv[])
{
	std::set_terminate(&Pipantas::OnTerminate);

	auto Args = std::vector<std::string>(argv, argv + argc);

	try
	{
		GameCoordinator.Initialize({}, Args);
		GameCoordinator.Run(Args);
		GameCoordinator.Shutdown({});
	}
	catch (...)
	{
		std::terminate();
	}

	return EXIT_SUCCESS;
}
