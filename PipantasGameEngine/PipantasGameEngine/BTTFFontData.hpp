#pragma once

#include <string>
#include "SDL_pixels.h"

class MResourceLoader;

enum class EFontRendering
{
	Solid,
	Blended
};

class BTTFFontData final
{
	public:
		BTTFFontData();
		~BTTFFontData() = default;

		SDL_Color Color;
		bool bKerning;
		EFontRendering Rendering;
		int Attributes;

		friend MResourceLoader;

	private:
		std::string Key;
};
