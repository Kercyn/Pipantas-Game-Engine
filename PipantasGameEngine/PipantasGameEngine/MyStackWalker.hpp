#pragma once

#include "StackWalker.h"

class MyStackWalker final : public StackWalker
{
public:
	MyStackWalker();
	~MyStackWalker() override = default;

protected:
	void OnOutput(LPCSTR szText) override;
};
