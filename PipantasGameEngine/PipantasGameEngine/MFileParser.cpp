#include <iostream>

#include "Managers.hpp"
#include "MFileParser.hpp"
#include "Defs.hpp"
#include "Utils.hpp"

MFileParser::MFileParser()
{
	File.exceptions(std::ifstream::failbit | std::ifstream::badbit);
}

void MFileParser::Initialize(const KeyGameCoordinator&, const std::vector<std::string>&)
{
	Logger.Log(ELogLevel::Info, "MFileParser initialized.");
}

void MFileParser::Shutdown(const KeyGameCoordinator&)
{
	Logger.Log(ELogLevel::Info, "MFileParser shutdown.");
}

void MFileParser::Open(std::string FilePath)
{
	// Close any previously open file.
	if (File.is_open())
	{
		File.close();
	}

	File.open(FilePath, std::ios::in);
}

CParserValue MFileParser::GetValue(std::string Key)
{
	if (!File.is_open())
	{
		Logger.Log(ELogLevel::Warning, "Attempted to get a value with key " + Key + " without opening a file first.");
	}

	std::string Line;

	while (getline(File, Line))
	{
		TCHAR FirstChar = Line.at(0);

		// We are not interested in comments or sections, so skip those.
		if ((FirstChar == '#') || (FirstChar == '['))
		{
			continue;
		}
		// If what we found our first key, return its value.
		else if (Line.substr(0, Key.length()) == Key)
		{
			std::string Result = Line.substr(Line.find_first_of('=') + 1);
			return Result;
		}
	}

	// We parsed the entire file and we didn't find the key, return the invalid key identifier.
	return Pipantas::Defs::INVALID_KEY;
}
