#pragma once

/*! \file */

#include <vector>
#include <string>

#include "BShader.hpp"
#include <gl/glew.h>

class BShaderProgram final
{
	public:
		BShaderProgram();
		~BShaderProgram();

		void Create();

		void AddShader(BShader Shader);

		void Link();

		void Use() const;

		void StopUsing() const;

		bool IsLinked() const;

		GLuint GetAttribLoc(const std::string& AttribName) const;

		GLint GetUniformLoc(const std::string& UniformName) const;

		operator GLuint() const;

	private:
		void Delete();

		//! All the shaders this program uses.
		std::vector<BShader> Shaders;

		GLuint ID;

		bool bIsLinked;
};
