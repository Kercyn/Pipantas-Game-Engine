#pragma once

/*! \file */

#include "MapElements.hpp"
#include "AActor.hpp"

#include <boost/optional.hpp>
#include <vector>
#include <map>
#include <string>

class MRenderer;
class MResourceLoader;
class MSceneSwitcher;

/*! \brief Parses and stores a TMX map.
*/
class CMap final
{
	public:
		explicit CMap() = default;
		CMap(const CMap&) = delete;
		CMap& operator=(const CMap&) = delete;

		~CMap() = default;


		// Object group type key
		static const std::string OG_TYPE;

		// Type constants for object layers
		static const std::string OG_DIALOGUES;
		static const std::string OG_ACTORS;
		static const std::string OG_CHARACTERS;
		static const std::string OG_SOUNDS;
		static const std::string OG_TEXT;
		static const std::string OG_TRIGGERS;

		// Name constants for property keys
		static const std::string PROP_SAP_X;
		static const std::string PROP_SAP_Y;
		static const std::string PROP_SCRIPTFILE;

		/*! \brief Loads a map.
		 \param ID The ID of the map to be loaded, as declared in the gamedata file.
		 */
		void Load(std::string ID_);

		int GetHeight() const;
		int GetWidth() const;
		glm::vec2 GetTileSize() const;
		const std::vector<MapElements::ObjectLayer>& GetObjectLayers() const;
		const std::vector<MapElements::ImageLayer>& GetImageLayers() const;
		boost::optional<std::string> GetProperty(std::string Key) const;
		
		//! Returns the associations between text objects and the font data they use.
		const std::map<std::string, std::string>& GetTextData() const;
		
		//! Returns all object layers of a specific type.
		std::vector<MapElements::ObjectLayer> GetObjectLayersOfType(MapElements::EObjectLayerType Type) const;

		friend MRenderer;
		friend MResourceLoader;
		friend MSceneSwitcher;

	private:
		//! Map ID as declared in the gamedata file.
		std::string ID;

		MapElements::Map Map;
		MapElements::Tileset Tileset;
		std::vector<MapElements::Layer> Layers;
		std::vector<MapElements::ObjectLayer> ObjectLayers;
		std::vector<MapElements::ImageLayer> ImageLayers;

		//! The map's tile colliders.
		std::vector<std::unique_ptr<PCollider>> Colliders;

		//! Associates the keys of text areas in the game with the FontData they use.
		std::map<std::string, std::string> TextData;

		/*! \brief Calculates each tile's metrics used by the renderer.
		Metrics include the tile's index and its offset.
		*/
		void CalculateTileMetrics();

		//! Generates the colliders based on the parsed data.
		void GenerateColliders();

		//! Parses the map's object layers and sets their type.
		void SetObjectLayerTypes();

		void LoadTextData();
};
