#pragma once

/*! \file */

#include "IManager.hpp"
#include "physfs.h"
#include "BPhysFSFilePtr.hpp"
#include "Pipanikos.hpp"

#include <string>
#include <vector>
#include <tchar.h>
#include <type_traits>

class MPhysFS final : public IManager
{
	public:
		MPhysFS() = default;
		~MPhysFS() = default;

		void Initialize(const KeyGameCoordinator&, const std::vector<std::string>& Args) override;
		void Shutdown(const KeyGameCoordinator&) override;

		int exists(std::string fname) const;
		int mount(std::string newDir, std::string mountPoint = std::string("/"), int appendToPath = 1) const;
		PHYSFS_sint64 fileLength(BPhysFSFilePtr& handle) const;


		template <typename T>
		void read(BPhysFSFilePtr& handle, std::vector<T>& buffer, PHYSFS_uint32 objSize, PHYSFS_uint32 objCount) const
		{
			PHYSFS_sint64 FileSize = fileLength(handle);
			
			buffer.resize(FileSize);
			PHYSFS_sint64 LengthRead = PHYSFS_read(handle, buffer.data(), 1, FileSize);

			// Add obligatory \0 and the end of the buffer if it's text.
			if (std::is_same<T, TCHAR>::value)
			{
				buffer.push_back('\0');
			}

			if (LengthRead != FileSize)
			{
				throw(Pipanikos("LengthRead: " + LengthRead + std::string("\n\tFileSize: ") + std::to_string(FileSize) +"\n\tPHYSFS last error: " + std::string(PHYSFS_getLastError()), __FILE__, __LINE__));
			}

			else if (LengthRead == -1)
			{
				throw(Pipanikos("LengthRead == -1\n\tPHYSFS last error: " + std::string(PHYSFS_getLastError()), __FILE__, __LINE__));
			}
		}
};
