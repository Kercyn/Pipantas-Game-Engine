#pragma once

/*! \file */

#include "luaconf.h"
#include "AText.hpp"

#include <string>
#include <tuple>

class SIText final
{
	public:
		AText* Text;

		SIText() = delete;
		SIText(std::string Key);
		~SIText() = default;

		std::tuple<LUA_NUMBER, LUA_NUMBER> GetPosition() const;
		std::string GetKey() const;
		std::string GetID() const;
		std::string GetProperty(std::string PropKey) const;
		bool IsEnabled() const;
		LUA_INTEGER GetColliderID(std::string Name) const;

		void Translate(LUA_NUMBER x, LUA_NUMBER y);
		void Rotate(LUA_NUMBER Angle);
		void SetPosition(LUA_NUMBER NewX, LUA_NUMBER NewY);
		void SetRotation(LUA_NUMBER NewRotation);
		void SetOrientation(LUA_NUMBER OrientX, LUA_NUMBER OrientY);
		void SetProperty(std::string PropKey, std::string Value);
		void SetText(std::string NewText);

		void Enable();
		void Disable();
};
