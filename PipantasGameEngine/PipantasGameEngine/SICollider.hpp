#pragma once

/*! \file */

#include "luaconf.h"
#include "SIActor.hpp"
#include "PCollider.hpp"

#include <string>
#include <cstdint>

class SICollider final
{
	public:
		SICollider() = delete;
		SICollider(LUA_INTEGER ID);
		~SICollider() = default;

		bool IsActive() const;
		bool IsEnabled() const;
		bool HasAttachedActor() const;
		std::string GetAttachedActorID() const;
		std::string GetName() const;
		std::tuple<LUA_NUMBER, LUA_NUMBER> GetSize() const;
		std::tuple<LUA_NUMBER, LUA_NUMBER> GetOffset() const;

		void Enable();
		void Disable();
		void SetOffset(LUA_NUMBER x, LUA_NUMBER y);

	private:
		PCollider* Collider;
};
