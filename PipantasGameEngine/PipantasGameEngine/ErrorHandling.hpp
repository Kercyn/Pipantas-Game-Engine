#pragma once

#include "Pipanikos.hpp"

#include <string>

/*! \brief GL function call wrapper

All GL calls go through this macro. The function is executed and then
checkGLError is called to check for errors.
ErrorManager must have been initialized before using GL_CALL!
*/

#ifdef _DEBUG
	#define GL_CALL(stmt) do { \
		stmt; \
		Pipantas::CheckGLError(#stmt); \
			} while (0,0);
#else
	#define GL_CALL(stmt) stmt;
#endif

namespace Pipantas
{
	[[noreturn]] void OnTerminate() noexcept;
	void ShowErrorMessageBox();
	void CheckGLError(std::string Statement);
}
