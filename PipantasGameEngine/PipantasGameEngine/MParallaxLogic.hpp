#pragma once

#include "IManager.hpp"
#include "CParallax.hpp"
#include "AccessKeys.hpp"

#include <string>

class MParallaxLogic final : public IManager
{
public:
	MParallaxLogic() = default;
	~MParallaxLogic() = default;

	void Initialize(const KeyGameCoordinator&, const std::vector<std::string>&) override;
	void Shutdown(const KeyGameCoordinator&) override;

	void OnCameraTranslate(const KeyCamera&, glm::vec2 Translation);
};
