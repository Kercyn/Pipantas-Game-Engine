#pragma once

/*! \file */

#include <string>
#include <map>
#include <boost\bimap.hpp>
#include <vector>

#include "IManager.hpp"
#include "AAudioArea.hpp"
#include "TIDGenerator.hpp"

enum class EPhysicsTrigger;

namespace Pipantas
{
	void MusicFinishedCallback();
	void ChannelFinishedCallback(int Channel);
}

class MAudioController final : public IManager
{
	public:
		MAudioController() = default;
		~MAudioController() override = default;

		// The command pattern could have been used a lot in this class, but I decided against it since 
		// mostly scripts will call the functions, so it's easier to simplify the signatures.

		void Initialize(const KeyGameCoordinator&, const std::vector<std::string>&) override;
		void Shutdown(const KeyGameCoordinator&) override;
		void Reset();

		void HaltAll() const;
		void PauseAll() const;
		void ResumeAll() const;
		void SetMasterVolume(int NewVolume) const;

		void FadeInArea(AAudioArea* const AudioArea);
		void FadeOutArea(AAudioArea* const AudioArea);

		//! Sets the current music position to \p Position seconds from the beginning.
		void SetMusicPosition(double Position) const;

		//! These volume functions accept volumes as percentages instead of the SDL_mixer range of 0-128.
		void SetMusicVolume(int NewVolume) const;
		void SetChannelVolume(int Channel, int NewVolume) const;
		
		void RewindMusic() const;
		void PauseMusic() const;
		void ResumeMusic() const;
		void HaltMusic() const;

		void PauseChannel(int Channel) const;
		void ResumeChannel(int Channel) const;
		void HaltChannel(int Channel) const;

		void AddOnTriggerEvent(EPhysicsTrigger Type, AAudioArea* AudioArea);
		void Tick();

		static const int INFINITE_LOOP = -1;
		static const int ALL_CHANNELS = -1;

		friend void Pipantas::MusicFinishedCallback();
		friend void Pipantas::ChannelFinishedCallback(int);

	private:
		TIDGenerator<64> ChannelPool;
		
		//! Associates AudioAreas to the channel they're currently using and vice versa.
		boost::bimap<AAudioArea*, int> ChannelOfArea;

		//! The AudioArea of type Music that is currently playing.
		AAudioArea* CurrentMusicArea;

		//! The AudioArea of type Music that should start playing once the current area finishes playing.
		AAudioArea* QueuedMusicArea;
};
