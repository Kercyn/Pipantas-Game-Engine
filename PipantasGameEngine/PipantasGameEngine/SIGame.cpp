#include "SIGame.hpp"
#include "MGameCoordinator.hpp"
#include "MScriptMiddleware.hpp"
#include "MActorHandler.hpp"
#include "MSceneSwitcher.hpp"
#include "CAnimation.hpp"
#include "AActor.hpp"
#include "ACharacter.hpp"
#include "AText.hpp"

SIGame::SIGame()
	: State(ScriptMiddleware.GetState())
{
}

LUA_NUMBER SIGame::GetDeltaTime() const
{
	return static_cast<LUA_NUMBER>(GameCoordinator.GetDeltaTime());
}

LUA_NUMBER SIGame::GetFixedDeltaTime() const
{
	return static_cast<LUA_NUMBER>(GameCoordinator.GetFixedDeltaTime());
}

void SIGame::GetCharacters(std::string VarName) const
{
	for (const auto Actor : ActorHandler.GetAllActors())
	{
		if (Actor->GetActorType() == EActorType::Character)
		{
			std::string Command = VarName + "[\"" + Actor->GetID() + "\"] = PCharacter.new(\"" + Actor->GetID() + "\")";
			State(Command.c_str());
		}
	}
}

void SIGame::GetTexts(std::string VarName) const
{
	for (const auto Actor : ActorHandler.GetAllActors())
	{
		if (Actor->GetActorType() == EActorType::Text)
		{
			std::string Command = VarName + "[\"" + Actor->GetID() + "\"] = PText.new(\"" + Actor->GetID() + "\")";
			State(Command.c_str());
		}
	}
}

void SIGame::GetActors(std::string VarName) const
{
	int i = 1;

	for (const auto Actor : ActorHandler.GetAllActors())
	{
		std::string Command = VarName + "[\"" + Actor->GetID() + "\"] = PActor.new(\"" + Actor->GetID() + "\")";
		State(Command.c_str());
	}
}

std::string SIGame::GetCurrentSceneKey() const
{
	return SceneSwitcher.GetCurrentScene()->GetKey();
}

void SIGame::ChangeScene(std::string SceneID) const
{
	SceneSwitcher.ChangeScene(SceneID);
}

void SIGame::SetDebugPhysics(bool Flag) const
{
	Renderer.SetDebugPhysics(Flag);
}

bool SIGame::Raycast(LUA_NUMBER OriginX, LUA_NUMBER OriginY, LUA_NUMBER DirectionX, LUA_NUMBER DirectionY, LUA_NUMBER MaxDistance, std::string LayerMask, bool bHitTriggers) const
{
	return CollisionEngine.Raycast(glm::vec2(OriginX, OriginY), glm::vec2(DirectionX, DirectionY), MaxDistance, LayerMask, bHitTriggers);
}

bool SIGame::IsKeyPressed(std::string Keycode) const
{
	return InputRegistrar.IsButtonPressed(SDL_GetKeyFromName(Keycode.c_str()));
}

void SIGame::SetMusicVolume(LUA_INTEGER NewVolume) const
{
	AudioController.SetMusicVolume(NewVolume);
}

void SIGame::SetChannelVolume(LUA_INTEGER Channel, LUA_INTEGER NewVolume) const
{
	AudioController.SetChannelVolume(Channel, NewVolume);
}

void SIGame::PrintLoadedResources() const
{
	std::string Command;

	for (const auto& Resource : ResourceStore.GetLoadedResources())
	{
		Command = "print(\"" + Resource + "\")";
		State(Command.c_str());
	}
}

void SIGame::PrintActorProperties(std::string ActorID) const
{
	std::string Command;

	for (const auto& Property : ActorHandler.GetActor(ActorID)->GetProperties())
	{
		Command = "print(\"" + Property.first + "\" .. \": \" .. \"" + Property.second + "\")";
		State(Command.c_str());
	}
}

LUA_INTEGER SIGame::AnimInfLoop() const
{
	return CAnimation::INFINITE_LOOP;
}
