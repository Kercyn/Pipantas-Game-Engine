#pragma once

/*! \file */

#include "IManager.hpp"
#include "AAudioArea.hpp"
#include "AccessKeys.hpp"
#include "BMixMusic.hpp"
#include "BMixChunk.hpp"

#include <cstddef>

class MAudioPlayback final : public IManager
{
	public:
		MAudioPlayback() = default;
		~MAudioPlayback() = default;

		void Initialize(const KeyGameCoordinator&, const std::vector<std::string>&) override;
		void Shutdown(const KeyGameCoordinator&) override;

		void FadeInMusic(const KeyAudioController&, BMixMusic& Music, int Loops, int FadeTime);
		void FadeOutMusic(const KeyAudioController&, int ms) const;

		void FadeInChannel(const KeyAudioController&, std::size_t Channel, BMixChunk& Chunk, int Loops, int FadeTime);
		void FadeOutChannel(const KeyAudioController&, std::size_t Channel, int FadeTime);
};
