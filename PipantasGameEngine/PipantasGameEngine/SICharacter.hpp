#pragma once

/*! \file */

#include "luaconf.h"
#include "ACharacter.hpp"

#include <string>
#include <tuple>

class SICharacter final
{
	public:
		SICharacter() = delete;
		SICharacter(std::string Key);
		~SICharacter() = default;

		void ChangeAnimation(std::string Key, int Loop, bool bPlayInReverse) const;

		std::tuple<LUA_NUMBER, LUA_NUMBER> GetPosition() const;
		std::tuple<LUA_NUMBER, LUA_NUMBER> GetOrientation() const;
		std::string GetKey() const;
		std::string GetID() const;
		std::string GetProperty(std::string PropKey) const;

		bool IsEnabled() const;
		bool HasCollider(std::string Name) const;
		LUA_INTEGER GetColliderID(std::string Name) const;

		void Translate(LUA_NUMBER x, LUA_NUMBER y);
		void Rotate(LUA_NUMBER Angle);
		void SetPosition(LUA_NUMBER NewX, LUA_NUMBER NewY);
		void SetRotation(LUA_NUMBER NewRotation);
		void SetOrientation(LUA_NUMBER OrientX, LUA_NUMBER OrientY);
		void SetProperty(std::string PropKey, std::string Value);

		void Enable();
		void Disable();

	private:
		ACharacter* Character;
};
