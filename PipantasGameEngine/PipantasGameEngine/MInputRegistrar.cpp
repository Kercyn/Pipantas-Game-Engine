#include "MInputRegistrar.hpp"
#include "SDL.h"
#include "Managers.hpp"

MInputRegistrar::MInputRegistrar()
	: ButtonPressed(std::map<int, bool>())
{
}

void MInputRegistrar::Initialize(const KeyGameCoordinator&, const std::vector<std::string>&)
{
	Logger.Log(ELogLevel::Info, "MInputRegistrar initialized.");
}

void MInputRegistrar::Shutdown(const KeyGameCoordinator&)
{
	Logger.Log(ELogLevel::Info, "MInputRegistrar shutdown.");
}

void MInputRegistrar::RegisterInput()
{
	static Uint32 WindowID = Renderer.GetWindowID();
	
	auto PlayerPos = ActorHandler.GetPlayer()->GetBody()->GetPosition();
	auto CamPos = Renderer.GetCamera().GetPosition();

	SDL_Event Event;

	while (SDL_PollEvent(&Event))
	{
		if (Event.type == SDL_QUIT)
		{
			GameCoordinator.RequestExit();
		}
		else if (Event.type == SDL_WINDOWEVENT)
		{
			if (Event.window.windowID == WindowID)
			{
				switch (Event.window.event)
				{
					case SDL_WINDOWEVENT_MINIMIZED:
						[[fallthrough]]
					case SDL_WINDOWEVENT_FOCUS_LOST:
						GameCoordinator.WindowLostFocus();
						break;

					case SDL_WINDOWEVENT_FOCUS_GAINED:
						GameCoordinator.WindowGainedFocus();
						break;

					case SDL_WINDOWEVENT_CLOSE:
						GameCoordinator.RequestExit();
						break;
				}
			}
		}
		else
		{
			switch (Event.type)
			{
				case SDL_KEYDOWN:
					ButtonPressed[Event.key.keysym.sym] = true;
					ScriptMiddleware.ProcessInputEvent(SDL_GetKeyName(Event.key.keysym.sym), EInputType::KeyDown);
					break;

				case SDL_KEYUP:
					ButtonPressed[Event.key.keysym.sym] = false;
					ScriptMiddleware.ProcessInputEvent(SDL_GetKeyName(Event.key.keysym.sym), EInputType::KeyUp);
					break;
			}
		}
	}
}

bool MInputRegistrar::IsButtonPressed(SDL_Keycode Keycode)
{
	return ButtonPressed[Keycode];
}
