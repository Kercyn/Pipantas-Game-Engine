#pragma once

/*! \file */

#include "IManager.hpp"
#include "SShape.hpp"
#include "PCollider.hpp"
#include "MPairGenerator.hpp"
#include "PCollisionLayer.hpp"
#include "TIDGenerator.hpp"
#include "glm\glm.hpp"

#include <functional>
#include <map>
#include <array>
#include <cstdint>

enum class EPhysicsTrigger
{
	Enter,
	Exit,
	Stay
};

class MCollisionEngine final : public IManager
{
	public:
		MCollisionEngine();
		~MCollisionEngine() = default;

		void Initialize(const KeyGameCoordinator&, const std::vector<std::string>&) override;
		void Shutdown(const KeyGameCoordinator&) override;
		
		//! Resets scene-specific containers so that they're ready for a new scene.
		void Reset();
		
		void Tick();

		const std::map<std::uint16_t, PCollider*>& GetColliders() const;
		const PCollider* const GetCollider(std::uint16_t ID) const;
		PCollisionLayer* const GetLayer(std::string Key) const;

		//! Registers a collider so that the manager can perform physics checks on it. If called when not loading it will throw an error.
		void RegisterCollider(PCollider* Collider, bool bIsColliderStatic = false);

		//! Returns true if \p a and \p b collide.
		static bool BroadAABBToAABB(const SAABB* const a, const SAABB* const b);
		
		static bool BroadCircleToShape(const SCircle& Circle, const SShape* const Shape);

		bool DoLayersCollide(const PCollisionLayer* const A, const PCollisionLayer* B) const;

		bool Raycast(glm::vec2 Origin, glm::vec2 Direction, float MaxDistance, std::string LayerMask, bool bHitTriggers = false) const;
		
		//! Returns all the SAP areas that the circle intersects.
		std::vector<std::reference_wrapper<const PSAP>> GetSAPsOfCircle(const glm::vec2& Center, float Radius) const;
		std::vector<std::reference_wrapper<const PSAP>> GetSAPsOfRay(const glm::vec2& Origin, const glm::vec2& Direction, float MaxDistance) const;

		typedef void (MCollisionEngine::*CollisionFunc)(PCollider*, PCollider*);

		//! We have a total of 2 shape types colliding in the narrow phase with each other, hence the size 2.
		std::array<std::array<CollisionFunc, 2>, 2> NarrowCollisionCallbacks;

		std::size_t GenerateColliderID();

		void UpdateColliders();

	private:
		glm::vec2 Gravity;

		// Collision layers, not to be confused with map layers
		std::map<std::string, std::unique_ptr<PCollisionLayer>> Layers;
		
		std::map<std::uint16_t, PCollider*> Colliders;

		//! Contains the translation vectors of each actor for the current frame.
		std::map<std::string, glm::vec2> TranslationVectors;

		TIDGenerator<10000> ColliderIDGenerator;

		static bool BroadCircleToAABB(const SCircle& Circle, const SAABB* const AABB);
		static bool BroadCircleToAABB(const glm::vec2& Center, float Radius, const SAABB& AABB);
		static bool BroadCircleToCircle(const SCircle& CircleA, const SCircle* const CircleB);
		static bool BroadCircleToOBB(const SCircle& Circle, const SOBB* const OBB);

		// Ray-shape intersection, used for raycasting
		static bool RayIntersectsAABB(glm::vec2 Origin, glm::vec2 Direction, float MaxDistance, const SAABB* AABB);
		static bool RayIntersectsCircle(glm::vec2 Origin, glm::vec2 Direction, float MaxDistance, const SCircle* Circle);
		static bool RayIntersectsOBB(glm::vec2 Origin, glm::vec2 Direction, float MaxDistance, const SOBB* OBB);


		// Narrow-phase collision functions
		void CircleToCircle(PCollider* a, PCollider* b);
		void CircleToOBB(PCollider* a, PCollider* b);

		void OBBToCircle(PCollider* a, PCollider* b);
		void OBBToOBB(PCollider* a, PCollider* b);

		//! Applies the gravity vector to all the actors' positions.
		void ApplyGravity() const;

		//! Moves all trigger colliders' "current" collision data to the "previous" vector.
		void UpdateColliderTriggers();

		//! Orders the pair manager to generate the potentially colliding pairs.
		void BroadPhase();
		
		//! Loops through the potentially colliding pairs and generates the scene manifolds.
		void NarrowPhase();

		//! Checks for trigger collider events and informs the scripting manager.
		void TriggerColliders();
		
		//! Loops through the scene manifolds and applies the appropriate forces to the colliding objects.
		void CollisionResolution();

		//! Parses the gamedata file and populates the collision layers map.
		void ParseLayerData();
};
