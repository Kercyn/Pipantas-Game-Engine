#pragma once

/*! \file */

#include <string>
#include <map>
#include <vector>

#include "SDL.h"
#include "SDL_keycode.h"

/*!	\brief Message box type.
\sa MsgboxButton
*/
enum class EMsgboxType : uint8_t
{
	OK_Cancel = 0,
	Yes_No,
	Info,
	Warning,
	Error
};

/*! \name Message box button flags
You can combine flags in order to create message boxes with multiple buttons.
\anchor EMsgboxButtonFlag
*/

namespace MsgboxButtonFlag
{
	constexpr uint8_t OK = (1u << 0),
		Cancel = (1u << 1),
		Yes = (1u << 2),
		No = (1u << 3);
};

/*!	\brief Contains details about a message box button.
\sa MsgboxType
*/
struct MsgboxButton
{
	/*! The ID of the button.
	\sa \ref MsgboxButtonFlags */
	uint8_t ID;
	std::string Caption;

	/*!	The SDL hotkey of the button
	\sa https://wiki.libsdl.org/SDL_Keycode */
	SDL_Keycode Hotkey;

	MsgboxButton(uint8_t ID_, std::string Caption_, SDL_Keycode Hotkey_)
		: ID(ID_), Caption(Caption_), Hotkey(Hotkey_)
	{}
};

/*!	\brief The MessageBox abstract class provides all the necessary attributes for a basic message box.

Since MessageBox is abstract, it does not provide any implementation details regarding
visuals, button sounds, icons, position or any other GUI-specific details.
\sa MessageBoxManager
*/
class WMessageBox
{
	public:
		WMessageBox(std::string Title_, std::string Message_, EMsgboxType Type_);
		virtual ~WMessageBox() = 0 {};

		/*!	Shows the message box.
		\return The ID of the button pressed.
		*/
		virtual int Show() const = 0;

	protected:
		std::string Title;
		std::string Message;
		EMsgboxType MsgboxType;

		// Contains the information needed for all the available message box buttons.
		static std::vector<MsgboxButton> MsgboxButtons;

		// Contains all message box varieties
		static std::map<EMsgboxType, uint8_t> ButtonDescription;

};
