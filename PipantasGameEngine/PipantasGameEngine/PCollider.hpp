#pragma once

/*! \file */

#include "SShape.hpp"
#include "SAABB.hpp"
#include "SOBB.hpp"
#include "SCircle.hpp"
#include "PCollisionLayer.hpp"
#include "glm\glm.hpp"
#include "glm\glm.hpp"

#include <memory>
#include <cstddef>
#include <string>
#include <vector>

class MCollisionEngine;
class MActorHandler;
class PPairManager;
class AActor;
class ACharacter;
class AAudioArea;
class CMap;

/*!
	None: The collider is not a trigger and participates in the collision checks.
	Programmable: The collider is a trigger and its behaviour is programmable by the developer through Lua scripts.
	Audio: The collider is a trigger and its behaviour is managed by the Audio Controller.

	The developer can't directly interact with Audio triggers, so a "trigger" from the developer's point of view
	is only the programmable one.
*/
enum class ETriggerType
{
	None,
	Programmable,
	Audio
};

class PCollider
{
	public:
		PCollider();
		PCollider(const PCollider& other);
		virtual ~PCollider() = default;

		PCollider& operator=(const PCollider& other);

		const std::vector<glm::vec2>& GetVertices() const;
		bool IsStatic() const;
		bool IsEnabledSelf() const;
		bool IsEnabledParent() const;
		bool IsActive() const;
		const PCollisionLayer* const PCollider::GetLayer() const;
		std::string GetName() const;
		std::uint16_t GetID() const;
		const AActor* const GetAttachedActor() const;
		const glm::vec2& GetOffset() const;
		const SAABB& GetHull() const;
		SShape* GetShape() const;
		virtual bool IsTrigger() const;

		//! Returns the collider's position in the world, taking into account the attached actor's position (if it exists).
		glm::vec2 GetAbsolutePosition() const;
		
		//! Applies appropriate rotation to the collider's shape (if it can be rotated) and updates the vertices.
		void ApplyRotation();

		void SetScale(glm::vec2 NewScale);
		void SetOffset(glm::vec2 NewOffset);
		void Enable();
		void Disable();
		void NeedsUpdate();

		friend MCollisionEngine;
		friend MActorHandler;
		friend PPairManager;
		friend AActor;
		friend ACharacter;
		friend AAudioArea;
		friend CMap;
		
	protected:

		ETriggerType TriggerType;
		bool bIsStatic;
		bool bEnabledSelf;
	
		//! Set when a change happens to the collider's rotation or scale.
		bool bNeedsUpdating;

		//! The collider's unique identifier.
		std::uint16_t ID;

		//! The collider's string unique identifier. Used to reference the collider in scripts etc.
		std::string Name;

		//! The actor that the collider is attached to. Is nullptr on tile colliders.
		AActor* AttachedActor;

		// Broadphase convex hull
		SAABB ConvexHull;

		PCollisionLayer* Layer;
		std::unique_ptr<SShape> Shape;

		//! Offset from the attached actor's center. If the collider has no attached actor, this is the collider's position.
		glm::vec2 Offset;

		//! Updates the collider's properties (hull, shape vertices etc).
		void Update();

		//! Updates the collider's current hull.
		void UpdateHull();
};
