#pragma once

/*! \file */

#include <string>
#include "WMessageBox.hpp"

/*! \brief MessageBox class using SDL. Can be created and shown to the user before any SDL/OpenGL initializations,
making it handy if you want to display error messages.
*/
class WSDLMessageBox final : public WMessageBox
{
	public:
		WSDLMessageBox(std::string Title_, std::string Message_, EMsgboxType MsgboxType_ = EMsgboxType::Error);
		~WSDLMessageBox();

		int Show() const override;

	private:
		uint32_t Flags;
};
