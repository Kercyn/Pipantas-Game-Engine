#pragma once

/*! \file */

/*! \brief Groups multiple T objects per axis.
*/
template <typename T>
struct TAxisDataGroup
{
	public:
		TAxisDataGroup() = default;
		~TAxisDataGroup() = default;

		T X;
		T Y;
};
