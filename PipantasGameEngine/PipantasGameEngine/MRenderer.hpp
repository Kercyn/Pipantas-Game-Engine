#pragma once

/*! \file */

#include <vector>
#include <memory>
#include <functional>
#include <cstdint>
#include <map>
#include <string>
#include <gl\glew.h>

#include "IManager.hpp"
#include "CCamera.hpp"
#include "Utils.hpp"
#include "BShaderProgram.hpp"
#include "SDL.h"

struct RenderData final
{
	void ClearContainers()
	{
		VertexCount.clear();
		VertexIndex.clear();
		ModelMatrix.clear();
	}

	GLuint VBO;
	GLuint InstanceVBO;
	GLuint VAO;
	GLsizei Instances;
	std::vector<GLsizei> VertexCount;
	std::vector<GLint> VertexIndex;
	std::vector<glm::mat4> ModelMatrix;
};

//! This struct holds all the information used by the UBO.
struct ShaderUniformData
{
	float CameraPosition[4][4];
};

/*! \brief Manager class responsible for rendering everything in the game.
*/
class MRenderer final : public IManager
{
	public:
		MRenderer();
		~MRenderer() override = default;

		void Initialize(const KeyGameCoordinator&, const std::vector<std::string>&) override;
		void Shutdown(const KeyGameCoordinator&) override;
		void Reset();

		void GenerateNewMapRenderData();
		void InitializeActorRenderData(std::string ID);
		void InitializeParallaxRenderData();

		void Render();

		//! Sets the debug physics flag that controls whether otherwise invisible shapes (such as collider bodies) are rendered.
		void SetDebugPhysics(bool Flag);

		Uint32 GetWindowFlags() const;
		Uint32 GetWindowID() const;

		CCamera& GetCamera();

	private:
		void GenerateRenderDataMapBuffers();

		void GenerateMapRenderData();
		void GenerateParallaxRenderData();
		void GenerateSAPRenderData();
		void GenerateActorRenderData();
		void GenerateDynamicColliderRenderData();
		void GenerateStaticColliderRenderData();

		//! Updates the UBO.
		void UpdateShaderUniforms();
		void UpdateParallaxInstanceVBO();
		void UpdateParallaxModelMatrix();

		void RenderFrontMapLayers() const;
		void RenderBackMapLayers() const;
		void RenderActors() const;
		void RenderSAP() const;
		void RenderBackParallax() const;
		void RenderFrontParallax() const;
		
		// To todo den isxuei alla to afhnw giati to egrapse o Pantelhs otan shkw8hka ligo ap' to pc. Kakos Pantelhs!
		//todo de douleuei. ta collider vertices einai panta entos o8onhs gia kapoio logo. Kapoia malakia exw kanei, na rwthsw ton Pantelh pou 8a 3erei
		void RenderColliders();

		CCamera Camera;

		// SDL-and-OpenGL-related stuff needed to create a screen and render to it.
		std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> SDLWindow;
		std::unique_ptr<SDL_Renderer, decltype(&SDL_DestroyRenderer)> SDLRenderer;
		SDL_GLContext GLContext;
		Uint32 WindowFlags;
		Uint32 ScreenFlags;
		int IMGInitFlags;

		// Screen settings
		int BPP;
		float FPS;
		float Gamma;
		SDL_Point Resolution;

		// Buffers
		GLuint UBO;
		ShaderUniformData UBOData;
		
		// Includes buffer data objects for everything but Actors and Parallaxes.
		std::map<std::string, RenderData> RenderDataMap;

		std::map<std::string, RenderData> ActorRenderData;
		std::map<std::string, RenderData> ParallaxRenderData;

		// Debug flags and settings
		//! If true, colliders, their convex hulls, SAP areas and contact points will be rendered on every frame.
		bool bDebugPhysics;
};
