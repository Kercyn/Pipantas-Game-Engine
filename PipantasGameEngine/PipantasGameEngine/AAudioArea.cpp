#include "AAudioArea.hpp"
#include "PTriggerCollider.hpp"
#include "MAudioController.hpp"
#include "Pipanikos.hpp"

const std::string AudioMapProperties::SOUND_ID = "SoundID";
const std::string AudioMapProperties::FADE_TIME = "FadeTime";
const std::string AudioMapProperties::LOOP = "Loop";
const std::string AudioMapProperties::ROLLOFF = "Rolloff";
const std::string AudioMapProperties::SHAPE = "Shape";
const std::string AudioMapProperties::TYPE = "Type";
const std::string AudioMapProperties::VOLUME = "Volume";
const std::string AudioMapProperties::PLAY_ON_ENTER = "PlayOnEnter";
const std::string AudioMapProperties::PLAY_AT_POS = "PlayAtPos";
const std::string AudioMapProperties::MIN_DISTANCE = "MinDistance";

const std::string AudioMapValues::TYPE_MUSIC = "music";
const std::string AudioMapValues::TYPE_SFX = "sfx";
const std::string AudioMapValues::ROLLOFF_NONE = "none";
const std::string AudioMapValues::ROLLOFF_LOG = "log";
const std::string AudioMapValues::ROLLOFF_LINEAR = "linear";
const std::string AudioMapValues::SHAPE_RECT = "rect";
const std::string AudioMapValues::SHAPE_CIRCLE = "circle";

const std::string AAudioArea::COLLIDER_NAME = "Area";

AAudioArea::AAudioArea()
{
	bIsEnabled = true;
}

bool AAudioArea::PlayOnEnter() const
{
	return bPlayOnEnter;
}

void AAudioArea::UpdateInformation(KeyActorHandler, const MapElements::Object& AreaSpawnerObj)
{
	auto SoundIDAttr = AreaSpawnerObj.Properties.GetProperty(AAudioArea::AudioProperties.SOUND_ID);
	auto RolloffAttr = AreaSpawnerObj.Properties.GetProperty(AAudioArea::AudioProperties.ROLLOFF);
	auto VolumeAttr = AreaSpawnerObj.Properties.GetProperty(AAudioArea::AudioProperties.VOLUME);
	auto LoopAttr = AreaSpawnerObj.Properties.GetProperty(AAudioArea::AudioProperties.LOOP);
	auto FadeTimeAttr = AreaSpawnerObj.Properties.GetProperty(AAudioArea::AudioProperties.FADE_TIME);
	auto PlayOnEnterAttr = AreaSpawnerObj.Properties.GetProperty(AAudioArea::AudioProperties.PLAY_ON_ENTER);
	auto PlayAtPosAttr = AreaSpawnerObj.Properties.GetProperty(AAudioArea::AudioProperties.PLAY_AT_POS);
	auto MinDistanceAttr = AreaSpawnerObj.Properties.GetProperty(AAudioArea::AudioProperties.MIN_DISTANCE);
	auto TypeAttr = AreaSpawnerObj.Properties.GetProperty(AAudioArea::AudioProperties.TYPE);

	if (!SoundIDAttr)
	{
		throw(Pipanikos("No " + AAudioArea::AudioProperties.SOUND_ID + " attribute found for " + AreaSpawnerObj.Name, __FILE__, __LINE__));
	}
	else
	{
		SoundID = *SoundIDAttr;

		if (PlayOnEnterAttr)
		{
			bPlayOnEnter = Utils::ToBool(*PlayOnEnterAttr);
		}
		else
		{
			bPlayOnEnter = true;
		}

		if (PlayAtPosAttr)
		{
			try
			{
				StartPosition = std::stod(*PlayAtPosAttr);
			}
			catch (const std::logic_error& e)
			{
				throw(Pipanikos(*LoopAttr + " is not a valid " + AudioProperties.PLAY_AT_POS + " attribute for " + AreaSpawnerObj.Name + "\n\t" + e.what(), __FILE__, __LINE__));
			}
		}
		else
		{
			StartPosition = 0.0;
		}

		if (RolloffAttr)
		{

			if (*RolloffAttr == "none")
			{
				Rolloff = ERolloffType::None;
			}
			else if (*RolloffAttr == "log")
			{
				Rolloff = ERolloffType::Logarithmic;
			}
			else if (*RolloffAttr == "linear")
			{
				Rolloff = ERolloffType::Linear;
			}
			else
			{
				Logger.Log(ELogLevel::Warning, "Invalid rolloff attribute for " + AreaSpawnerObj.Name + std::string(". Defaulting to none."));
				Rolloff = ERolloffType::None;
			}
		}
		else
		{
			Rolloff = ERolloffType::None;
		}

		if (TypeAttr)
		{
			if (*TypeAttr == "music")
			{
				AreaType = EAreaType::Music;
			}
			else if (*TypeAttr == "sfx")
			{
				AreaType = EAreaType::SFX;
			}
			else
			{
				throw(Pipanikos(*TypeAttr + " is an invalid type attribute for " + AreaSpawnerObj.Name, __FILE__, __LINE__));
			}
		}
		else
		{
			AreaType = EAreaType::Music;
		}

		try
		{
			if (VolumeAttr)
			{
				Volume = std::stoi(*VolumeAttr);
			}
			else
			{
				Volume = SDL_MIX_MAXVOLUME;
			}

			if (FadeTimeAttr)
			{
				FadeTime = std::stoi(*FadeTimeAttr);
			}
			else
			{
				FadeTime = 0;
			}

			if (MinDistanceAttr)
			{
				MinDistance = std::stof(*MinDistanceAttr);
			}
			else
			{
				MinDistance = 0.0f;
			}
			
			if (LoopAttr)
			{
				if (*LoopAttr == "inf")
				{
					Loops = MAudioController::INFINITE_LOOP;
				}
				else
				{
					Loops = std::stoi(*LoopAttr);
				}
			}
			else
			{
				Loops = MAudioController::INFINITE_LOOP;
			}
		}
		catch (const std::logic_error& e)
		{
			throw (Pipanikos((*LoopAttr + " is not a valid loop attribute for " + AreaSpawnerObj.Name + "\n\t" + e.what()).c_str(), __FILE__, __LINE__));
		}
	}

	CopyProperties(AreaSpawnerObj);

	// Set additional properties
	Rigidbody.Mass = 0.0f;
	Rigidbody.bUsesGravity = false;

	for (auto& Collider : Colliders)
	{
		Collider->bIsStatic = true;
	}
}
