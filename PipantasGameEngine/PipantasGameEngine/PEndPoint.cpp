#include "PEndPoint.hpp"

#include <cstdint>
#include <functional>


PEndPoint::PEndPoint()
	: Value(0.0f), ColliderID(-1), bIsMax(false)
{}

PEndPoint::PEndPoint(std::uint16_t ColliderID_, float Value_, bool bIsMax_)
	: ColliderID(ColliderID_), Value(Value_), bIsMax(bIsMax_)
{}

bool PEndPoint::operator<(const PEndPoint& rhs) const
{
	return (Value < rhs.Value);
}

bool PEndPoint::operator>(const PEndPoint& rhs) const
{
	return (Value > rhs.Value);
}

bool PEndPoint::operator==(const PEndPoint& rhs) const
{
	return (ColliderID == rhs.ColliderID) && (bIsMax == rhs.bIsMax);
}
