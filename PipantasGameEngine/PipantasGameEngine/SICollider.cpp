#include "SICollider.hpp"
#include "Managers.hpp"
#include "glm\glm.hpp"
#include "Pipanikos.hpp"

SICollider::SICollider(LUA_INTEGER ID)
{
	for (const auto& KVP : CollisionEngine.GetColliders())
	{
		auto CastID = static_cast<std::uint16_t>(ID);

		if (KVP.second->GetID() == CastID)
		{
			Collider = KVP.second;
		}
	}

	if (Collider == nullptr)
	{
		throw(Pipanikos("Unable to find collider " + static_cast<unsigned>(ID), __FILE__, __LINE__));
	}
}

std::string SICollider::GetAttachedActorID() const
{
	if (auto AttachedActor = Collider->GetAttachedActor())
	{
		return AttachedActor->GetID();
	}
	else
	{
		throw(Pipanikos("Collider " + Collider->GetName() + " has no attached actor.", __FILE__, __LINE__));
	}
}

bool SICollider::IsActive() const
{
	return (Collider->IsEnabledSelf() && Collider->IsEnabledParent());
}

bool SICollider::IsEnabled() const
{
	return Collider->IsEnabledSelf();
}

bool SICollider::HasAttachedActor() const
{
	return (Collider->GetAttachedActor() != nullptr);
}

std::string SICollider::GetName() const
{
	return Collider->GetName();
}

std::tuple<LUA_NUMBER, LUA_NUMBER> SICollider::GetSize() const
{
	auto Size = Collider->GetShape()->GetSize();
	return std::make_tuple(Size.x, Size.y);
}

std::tuple<LUA_NUMBER, LUA_NUMBER> SICollider::GetOffset() const
{
	auto Offset = Collider->GetOffset();
	return std::tuple<LUA_NUMBER, LUA_NUMBER>(Offset.x, Offset.y);
}

void SICollider::Enable()
{
	Collider->Enable();
}

void SICollider::Disable()
{
	Collider->Disable();
}

void SICollider::SetOffset(LUA_NUMBER x, LUA_NUMBER y)
{
	Collider->SetOffset(glm::vec2(x, y));
}
