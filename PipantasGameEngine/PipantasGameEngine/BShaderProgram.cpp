#include "BShaderProgram.hpp"
#include "Managers.hpp"
#include "Pipanikos.hpp"
#include "ErrorHandling.hpp"

BShaderProgram::BShaderProgram()
	: bIsLinked(false)
{
}

BShaderProgram::~BShaderProgram()
{
	Delete();
}

void BShaderProgram::Create()
{
	ID = glCreateProgram();

	if (ID == 0)
	{
		throw(Pipanikos("Failed to create shader program!", __FILE__, __LINE__));
	}
}

void BShaderProgram::Delete()
{
	GL_CALL(glUseProgram(0));
	GL_CALL(glDeleteProgram(ID));

	if (bIsLinked)
	{
		for (auto& Shader : Shaders)
		{
			GL_CALL(glDetachShader(ID, Shader));
			Shader.Delete({});
		}
	}

	Shaders.clear();
	bIsLinked = false;
}

void BShaderProgram::AddShader(BShader Shader)
{
	Shaders.push_back(Shader);
	GL_CALL(glAttachShader(ID, Shader));
}

void BShaderProgram::Link()
{
	GL_CALL(glLinkProgram(ID));
	
	GLint LinkStatus;
	GL_CALL(glGetProgramiv(ID, GL_LINK_STATUS, &LinkStatus));

	if (LinkStatus == GL_TRUE)
	{
		for (auto& Shader : Shaders)
		{
			GL_CALL(glDetachShader(ID, Shader));
			Shader.Delete({});
		}

		Shaders.clear();
		bIsLinked = true;
	}
	else
	{
		std::string ErrorMessage("Program failed to link.\nShader paths: ");

		for (const auto& Shader : Shaders)
		{
			ErrorMessage.append(Shader.GetPath() + "\n");
		}

		// Get shader log info
		GLint LogLength;
		GL_CALL(glGetProgramiv(ID, GL_INFO_LOG_LENGTH, &LogLength));

		std::vector<GLchar> InfoLog(LogLength);
		GL_CALL(glGetProgramInfoLog(ID, LogLength, &LogLength, &InfoLog[0]));

		GL_CALL(glDeleteProgram(ID));

		ErrorMessage.append(InfoLog.data());

		throw(Pipanikos(ErrorMessage, __FILE__, __LINE__));
	}
}

void BShaderProgram::Use() const
{
	GL_CALL(glUseProgram(ID));
}

void BShaderProgram::StopUsing() const
{
	GL_CALL(glUseProgram(0));
}

bool BShaderProgram::IsLinked() const
{
	return bIsLinked;
}

GLuint BShaderProgram::GetAttribLoc(const std::string& AttribName) const
{
	GLint AttribLoc = glGetAttribLocation(ID, AttribName.c_str());

	if (AttribLoc == -1)
	{
		std::string ErrorMessage = "Attribute location not found for " + AttribName + " on program " + std::to_string(ID) + ".\nShaders:\n";
		
		for (const auto& Shader : Shaders)
		{
			ErrorMessage.append(Shader.GetPath() + "\n");
		}

		throw(Pipanikos(ErrorMessage, __FILE__, __LINE__));
	}

	return AttribLoc;
}

GLint BShaderProgram::GetUniformLoc(const std::string& UniformName) const
{
	GLint UniformLoc = glGetUniformLocation(ID, UniformName.c_str());

	if (UniformLoc == -1)
	{
		std::string ErrorMessage = "Attribute location not found for " + UniformName + " on program " + std::to_string(ID) + ".\nShaders:\n";

		for (const auto& Shader : Shaders)
		{
			ErrorMessage.append(Shader.GetPath() + "\n");
		}

		throw(Pipanikos(ErrorMessage, __FILE__, __LINE__));
	}

	return UniformLoc;
}

BShaderProgram::operator GLuint() const
{
	return ID;
}
