#pragma once

/*! \file */

#include <string>
#include <map>
#include <vector>
#include <memory>

#include "PCollider.hpp"
#include "SDL.h"
#include "glm\glm.hpp"
#include "BTexture.hpp"
#include "MapElements.hpp"
#include "PRigidbody.hpp"
#include "AccessKeys.hpp"

class MActorHandler;

struct TextureCoords final
{
	TextureCoords() = default;
	TextureCoords(glm::vec2 Start_, glm::vec2 End_)
		: Start(Start_), End(End_)
	{}

	glm::vec2 Start;
	glm::vec2 End;
};

struct ActorMapProperties final
{
	static const std::string A_ID;
	static const std::string KEY;
	static const std::string CONSTRAINTS;
	static const std::string ENABLED;
	static const std::string SCALE_X;
	static const std::string SCALE_Y;
	static const std::string USES_GRAVITY;
	static const std::string MASS;
};

struct ActorMapValues final
{
	static const std::string CONSTRAINTS_NONE;
	static const std::string CONSTRAINTS_X;
	static const std::string CONSTRAINTS_Y;
	static const std::string CONSTRAINTS_ALL;
};

enum class EActorType
{
	Actor,
	Audio,
	Character,
	Text
};

/*! \brief An Actor is a character inside the game, either a player character or an NPC.
Actors must follow the directory structure described here.
*/
class AActor
{
	public:
		AActor();
		AActor(const AActor& Other);
		virtual ~AActor() = default;

		virtual AActor& operator=(const AActor& Other);
		virtual AActor& operator=(AActor&& Other);
		
		virtual void Update();

		std::string GetID() const;
		std::string GetKey() const;
		EActorType GetActorType() const;
		bool IsEnabled() const;
		glm::vec2 GetOrientation() const;
		PCollider const* const GetCollider(std::string Name) const;
		const PRigidbody& GetRigidbody() const;
		const SShape* const GetBody()const;
		std::string GetProperty(std::string PropKey) const;
		const std::map<std::string, std::string>& GetProperties() const;
		const std::vector<std::unique_ptr<PCollider>>& GetColliders() const;
		std::vector<std::unique_ptr<PCollider>>& GetColliders(KeyActorHandler);
		virtual TextureCoords GetTexCoords() const;

		/*! \brief Returns the texture that the actor should use in this frame. 
		For actors, it's their "normal" texture. For characters, it's the texture of their current animation.
		*/
		virtual BTexture GetCurrentTexture() const;

		void SetID(KeyActorHandler, std::string ID_);
		void SetKey(KeyActorHandler, std::string Key_);
		void SetActorType(KeyActorHandler, EActorType ActorType_);
		void SetBody(KeyActorHandler, std::unique_ptr<SShape> Body_);

		//! Overrides actor attributes found in the actor's properties file with spawner-specified attributes (such as scale, mass, constraints etc.).
		virtual void OverridePropFileAttributes(KeyActorHandler, const MapElements::Object& SpawnerObj);

		//! Updates the actor's attributes based on the data of the map's spawn object.
		virtual void UpdateInformation(KeyActorHandler, const MapElements::Object& SpawnerObj);

		//! Loads Actor information from its properties file and attaches actor colliders. Does NOT load actor assets.
		virtual void LoadProperties(std::string Key);

		void Enable();
		void Disable();

		void Translate(const glm::vec2& Offset);
		void Translate(float OffsetX, float OffsetY);
		void SetPosition(const glm::vec2& NewPosition);
		void SetPosition(float NewX, float NewY);
		void Rotate(float Angle);
		void SetRotation(float NewRotation);
		void SetScale(const glm::vec2& NewScale);
		void Scale(float x, float y);

		void SetOrientation(const glm::vec2& NewOrientation);
		void SetOrientation(float OrientX, float OrientY);

		void SetProperty(std::string PropKey, std::string Value);
		void RemoveProperty(std::string PropKey);

		static const ActorMapProperties ActorProperties;
		static const ActorMapValues ActorValues;

	protected:

		//! Used to identify the type of the Actor.
		EActorType ActorType;

		//! If false, the actor will not be rendered or be checked for collisions.
		bool bIsEnabled;

		//! If false, the actor is not inside the camera's field of view.
		bool bIsOnCamera;

		//! The unique identifier of the actor.
		std::string ID;

		//! The actor's unique key as declared in the gamedata file. Multiple actors of the same type will have the same key.
		std::string Key;

		//! The actor's body that defines its shape, size, position etc. Bodies are either OBBs or Circles (not AABBs).
		std::unique_ptr<SShape> Body;

		PRigidbody Rigidbody;

		//! Texture orientation of the actor; whether it's facing up/down and left/right.
		glm::vec2 Orientation;

		//! User-defined properties in scripts.
		std::map<std::string, std::string> Properties;

		std::vector<std::unique_ptr<PCollider>> Colliders;

		//! Copies all the properties of the spawner object to the Actor's properties.
		virtual void CopyProperties(const MapElements::Object& SpawnerObj);
};
