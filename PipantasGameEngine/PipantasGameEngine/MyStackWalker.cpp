#include "MyStackWalker.hpp"
#include "Defs.hpp"

#include <ostream>
#include <fstream>

MyStackWalker::MyStackWalker()
	: StackWalker()
{}

void MyStackWalker::OnOutput(LPCSTR szText)
{
	std::ofstream Log;

	Log.open(Pipantas::Defs::FILE_LOG, std::fstream::app);

	if (Log.is_open())
	{
		Log << szText;
	}
}
