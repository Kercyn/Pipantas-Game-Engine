#include "SAABB.hpp"
#include "Managers.hpp"
#include "Utils.hpp"
#include "glm\gtx\projection.hpp"

SAABB::SAABB()
	: Min(glm::vec2()), Max(glm::vec2())
{
	Type = EShapeType::AABB;
	Vertices.resize(4);
	Normals.resize(4);
}

std::unique_ptr<SShape> SAABB::Clone() const
{
	return std::make_unique<SAABB>(*this);
}

Line SAABB::ProjectInto(const glm::vec2& Axis) const
{
	auto First = GetSupportPoint(Axis);
	First = glm::proj(First, Axis);

	auto Second = GetSupportPoint(-Axis);
	Second = glm::proj(Second, -Axis);

	return Line(First, Second);
}

bool SAABB::IsPointInside(const glm::vec2& Point) const
{
	return ((Min.x <= Point.x) && (Point.x <= Max.x)
		&& (Min.y <= Point.y) && (Point.y <= Max.y));
}

glm::mat4 SAABB::GetModelMatrix() const
{
	return TranslationMat * glm::mat4(ScaleMat);
}

glm::vec2 SAABB::GetSize() const
{
	return glm::vec2(Max.x - Min.x, Max.y - Min.y);
}

glm::vec2 SAABB::GetSupportPoint(const glm::vec2& Direction) const
{
	float BestProjection = -std::numeric_limits<float>::max();
	glm::vec2 BestVertex;

	for (const auto& Vertex : Vertices)
	{
		float Projection = glm::dot(Vertex, Direction);

		if (Projection > BestProjection)
		{
			BestProjection = Projection;
			BestVertex = Vertex;
		}
	}

	return BestVertex + Position;
}

void SAABB::SetBoundaries(const glm::vec2& Min_, const glm::vec2& Max_)
{
	Min = Min_;
	Max = Max_;

	UpdateVerticesAndNormals();
}

void SAABB::SetMin(float x, float y)
{
	Min.x = x;
	Min.y = y;

	UpdateVerticesAndNormals();
}

void SAABB::SetMax(float x, float y)
{
	Max.x = x;
	Max.y = y;

	UpdateVerticesAndNormals();
}

void SAABB::SetSize(glm::vec2 NewSize)
{
	float HalfWidth = NewSize.x / 2.0f;
	float HalfHeight = NewSize.y / 2.0f;

	SetMin(-HalfWidth, -HalfHeight);
	SetMax(HalfWidth, HalfHeight);
}

const glm::vec2& SAABB::GetMin() const
{
	return Min;
}

const glm::vec2& SAABB::GetMax() const
{
	return Max;
}

void SAABB::UpdateVerticesAndNormals()
{
	Vertices[0].x = Min.x;
	Vertices[0].y = Min.y;

	Vertices[1].x = Min.x;
	Vertices[1].y = Max.y;

	Vertices[2].x = Max.x;
	Vertices[2].y = Max.y;

	Vertices[3].x = Max.x;
	Vertices[3].y = Min.y;

	// AABBs don't rotate by definition, so normals never change and thus need no updating.
}
