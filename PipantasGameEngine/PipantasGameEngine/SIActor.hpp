#pragma once

/*! \file */

#include "luaconf.h"
#include "AActor.hpp"
#include "selene.h"

#include <string>
#include <tuple>

class SIActor final
{
	public:
		AActor* Actor;

		SIActor() = delete;
		SIActor(std::string Key);
		 ~SIActor() = default;

		 std::tuple<LUA_NUMBER, LUA_NUMBER> GetPosition() const;
		 std::tuple<LUA_NUMBER, LUA_NUMBER> GetOrientation() const;
		 std::string GetKey() const;
		 std::string GetID() const;
		 std::string GetProperty(std::string PropKey) const;
		 bool IsEnabled() const;
		 bool HasCollider(std::string Name) const;
		 LUA_NUMBER GetColliderID(std::string Name) const;

		 void Translate(LUA_NUMBER x, LUA_NUMBER y);
		 void Rotate(LUA_NUMBER Radians);
		 void SetPosition(LUA_NUMBER NewX, LUA_NUMBER NewY);
		 void SetRotation(LUA_NUMBER NewRotation);
		 void SetOrientation(LUA_NUMBER OrientX, LUA_NUMBER OrientY);

		 void SetProperty(std::string PropKey, std::string Value);

		 void Enable();
		 void Disable();
};
