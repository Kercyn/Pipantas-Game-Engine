#include "CAnimation.hpp"
#include "Utils.hpp"
#include "Managers.hpp"

CAnimation::CAnimation()
	: TotalFrames(1), CurrentFrame(0), TimesLooped(0), Triggers(std::map<std::uint8_t, std::string>()), bResetFrames(false)
{
}

void CAnimation::Play()
{
	// How many frames have been played in the current animation.
	// This is needed because animations that play in reverse trigger animation triggers incorrectly.
	// (for example, if an animation that has 10 frames and an animation trigger on the 10th frame,
	// it will trigger on the first frame if played in reverse).
	std::uint8_t FrameNo;

	FrameNo = (bPlayInReverse) ? TotalFrames - CurrentFrame - 1: CurrentFrame;

	if ((Triggers.count(FrameNo) == 1) && (TimeOnThisFrame < Utils::EPSILON))
	{
		std::string Function = Triggers.at(FrameNo);
		std::size_t Colon = Function.find_first_of(':');

		//Check if we need to call a free function or a class method
		if (Colon == std::string::npos)
		{
			ScriptMiddleware.Run<std::string>(Function, CharacterID);
		}
		else
		{
			ScriptMiddleware.Run<std::string>(Function.substr(0, Colon), Function.substr(Colon + 1, Function.size()), CharacterID);
		}
	}

	bool bCanLoop = (Loop == CAnimation::INFINITE_LOOP) || (TimesLooped < Loop);

	if (bCanLoop)
	{
		if ((TimeOnThisFrame - TimePerFrame) > Utils::EPSILON)
		{
			if (bPlayInReverse)
			{
				if (CurrentFrame == 0)
				{
					TimesLooped++;
					bCanLoop = (Loop == CAnimation::INFINITE_LOOP) || (TimesLooped < Loop);
					bResetFrames = true;
				}

				if (bResetFrames)
				{
					if (bCanLoop)
					{
						CurrentFrame = TotalFrames - 1;
						bResetFrames = false;
					}
				}
				else
				{
					CurrentFrame--;
				}
			}
			else
			{
				if (CurrentFrame == TotalFrames - 1)
				{
					TimesLooped++;
					bCanLoop = (Loop == CAnimation::INFINITE_LOOP) || (TimesLooped < Loop);
					bResetFrames = true;
				}

				if (bResetFrames)
				{
					if (bCanLoop)
					{
						CurrentFrame = 0;
						bResetFrames = false;
					}
				}
				else
				{
					CurrentFrame++;
				}
			}

			TimeOnThisFrame = 0.0f;
		}
		else
		{
			TimeOnThisFrame += GameCoordinator.GetDeltaTime();
		}
	}
}

std::string CAnimation::GetKey() const
{
	return Key;
}

int CAnimation::GetTotalFrames() const
{
	return TotalFrames;
}

float CAnimation::GetTimePerFrame() const
{
	return TimePerFrame;
}

int CAnimation::GetCurrentFrame() const
{
	return CurrentFrame;
}

float CAnimation::GetNormFrameWidth() const
{
	return NormFrameWidth;
}
