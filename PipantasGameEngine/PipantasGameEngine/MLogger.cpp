#include "MLogger.hpp"
#include "Utils.hpp"
#include "Defs.hpp"
#include "WSDLMessageBox.hpp"
#include <gl/glew.h>

#include <fstream>
#include <iostream>
#include <memory>
#include <ctime>
#include <chrono>

const std::map<ELogLevel, std::string> MLogger::LevelStringMap = {
	{ ELogLevel::Trace, "TRACE" },
	{ ELogLevel::Debug, "DEBUG" },
	{ ELogLevel::Info, "INFO" },
	{ ELogLevel::Warning, "WARNING" },
	{ ELogLevel::Error, "ERROR" }
};

MLogger::MLogger()
	: DefaultLoggingLevel(ELogLevel::Warning), CurrentLoggingLevel(DefaultLoggingLevel)
{}

void MLogger::Initialize(const KeyGameCoordinator&, const std::vector<std::string>& Args)
{
	for (int i = 0; i < Args.size(); i++)
	{
		if (Args.at(i) == "-loglevel")
		{
			if ((i + 1) < Args.size())
			{
				const auto& Level = Args.at(i + 1);
				if (Level == "trace")
				{
					CurrentLoggingLevel = ELogLevel::Trace;
				}
				else if (Level == "debug")
				{
					CurrentLoggingLevel = ELogLevel::Debug;
				}
				else if (Level == "info")
				{
					CurrentLoggingLevel = ELogLevel::Info;
				}
				else if (Level == "warning")
				{
					CurrentLoggingLevel = ELogLevel::Warning;
				}
				else if (Level == "error")
				{
					CurrentLoggingLevel = ELogLevel::Error;
				}
				else
				{
					CurrentLoggingLevel = DefaultLoggingLevel;
					Log(ELogLevel::Warning, "Invalid -loglevel argument (" + Level + "), falling back to default.");
				}
			}
			else
			{
				CurrentLoggingLevel = DefaultLoggingLevel;
				Log(ELogLevel::Warning, "-loglevel argument missing, falling back to default.");
			}
		}
	}

	Log(ELogLevel::Info, "MLogger initialized.");
}

void MLogger::Shutdown(const KeyGameCoordinator&)
{
	Log(ELogLevel::Info, "MLogger shutdown.");
}

void MLogger::Log(ELogLevel Level, std::string Message) const
{
	if (Level >= CurrentLoggingLevel)
	{
		std::ofstream Log;

		Log.open(Pipantas::Defs::FILE_LOG, std::fstream::app);

		if (Log.is_open() && Log.good())
		{
			auto TimeNow = std::chrono::system_clock::now();
			std::time_t DateTime = std::chrono::system_clock::to_time_t(TimeNow);
			std::string DateTimeStr = std::ctime(&DateTime);
			DateTimeStr = Utils::StripCRLF(DateTimeStr);

			Log << DateTimeStr;
			Log.width(3);
			Log << "[" << LevelStringMap.at(Level) << "] ";
			Log << Message;
			Log << std::endl;
		}
		else
		{
			WSDLMessageBox ErrorMessagebox(Pipantas::Defs::GAME_TITLE, "Could not open " + Pipantas::Defs::FILE_LOG + "\nFile error: " + strerror(errno));
			ErrorMessagebox.Show();
		}
	}
}
