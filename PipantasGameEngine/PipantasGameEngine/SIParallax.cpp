#include "SIParallax.hpp"
#include "Managers.hpp"

SIParallax::SIParallax(std::string Key)
	: Parallax(ParallaxStore.GetParallax(Key))
{
}

void SIParallax::SetSpeed(LUA_NUMBER NewSpeed) const
{
	Parallax.Speed = NewSpeed;
}

void SIParallax::SetEnabled(bool Flag) const
{
	Parallax.bIsEnabled = Flag;
}

void SIParallax::SetRenderFront(bool Flag) const
{
	Parallax.bRenderFront = Flag;
}

LUA_NUMBER SIParallax::GetSpeed() const
{
	return static_cast<LUA_NUMBER>(Parallax.Speed);
}

bool SIParallax::IsEnabled() const
{
	return Parallax.bIsEnabled;
}

bool SIParallax::IsRenderedFront() const
{
	return Parallax.bRenderFront;
}
