#include "CScene.hpp"
#include "rapidxml.hpp"
#include "Managers.hpp"
#include "Pipanikos.hpp"

#include <tchar.h>

void CScene::Load(std::string SceneKey)
{
	using namespace rapidxml;
	
	Key = SceneKey;
	
	std::string GameDataRaw = ResourceStore.RequestString(ResourceStore.Keys.GAMEDATA_FILE_KEY);
	std::vector<TCHAR> GameData = std::vector<TCHAR>(GameDataRaw.c_str(), GameDataRaw.c_str() + GameDataRaw.length() + 1);

	bool bFoundScene = false;

	xml_document<> XMLDoc;
	XMLDoc.parse<0>(GameData.data());

	auto SceneNode = XMLDoc.first_node("game")->first_node("scenes")->first_node("scene");

	// Find the scene that we need
	while ((SceneNode != nullptr) && (! bFoundScene))
	{
		if (SceneNode->first_attribute("key")->value() != Key)
		{
			SceneNode = SceneNode->next_sibling("scene");
		}
		else
		{
			bFoundScene = true;
		}
	}

	if (! bFoundScene)
	{
		throw(Pipanikos(Key + " scene not found", __FILE__, __LINE__));
	}

	LevelMap.Load(SceneNode->first_attribute("map")->value());
}

const std::map<std::string, std::string>& CScene::GetProperties() const
{
	return Properties;
}

std::string CScene::GetKey() const
{
	return Key;
}
