#pragma once

/*! \file */

#include <vector>
#include <string>
#include "AccessKeys.hpp"

/*! \brief The base class for all managers.

A Manager class implements a core functionality of the engine, such as file I/O or collision detection.
Be extra careful on what you put inside the constructor. Some initializations (ex. OpenGL ones)
must not be done on the constructor (use the Initialize function in this case).
*/
class IManager
{
	public:
		IManager(){}
		virtual ~IManager() = 0 {}

		virtual void Initialize(const KeyGameCoordinator& Key, const std::vector<std::string>& Args = {}) = 0 {}
		virtual void Shutdown(const KeyGameCoordinator&) = 0 {}
};
