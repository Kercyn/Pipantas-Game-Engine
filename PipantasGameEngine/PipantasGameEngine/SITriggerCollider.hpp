#pragma once

/*! \file */

#include "luaconf.h"
#include "SIActor.hpp"
#include "PTriggerCollider.hpp"

#include <string>
#include <cstdint>

class SITriggerCollider final
{
	public:
		SITriggerCollider() = delete;
		SITriggerCollider(LUA_INTEGER ID);
		~SITriggerCollider() = default;

		std::string GetAttachedActorID() const;
		std::string GetProperty(std::string ID) const;
		bool IsActive() const;
		bool IsEnabled() const;
		bool HasAttachedActor() const;
		std::string GetName() const;
		std::tuple<LUA_NUMBER, LUA_NUMBER> GetSize() const;
		std::tuple<LUA_NUMBER, LUA_NUMBER> GetOffset() const;

		void Enable();
		void Disable();
		void SetOffset(LUA_NUMBER x, LUA_NUMBER y);

	private:
		PTriggerCollider* Collider;
};
