#pragma once

/*! \file */

#include "luaconf.h"
#include "Selene.h"
#include "SIActor.hpp"
#include "SICharacter.hpp"

#include <vector>
#include <string>

class SIGame final
{
	public:
		SIGame();
		~SIGame() = default;

		LUA_NUMBER GetDeltaTime() const;
		LUA_NUMBER GetFixedDeltaTime() const;
		void GetCharacters(std::string VarName) const;
		void GetTexts(std::string VarName) const;
		void GetActors(std::string VarName) const;
		std::string GetCurrentSceneKey() const;
		void ChangeScene(std::string SceneID) const;
		void SetDebugPhysics(bool Flag) const;
		bool Raycast(LUA_NUMBER OriginX, LUA_NUMBER OriginY, LUA_NUMBER DirectionX, LUA_NUMBER DirectionY, LUA_NUMBER MaxDistance, std::string LayerMask, bool bHitTriggers) const;
		bool IsKeyPressed(std::string Keycode) const;
		void SetMusicVolume(LUA_INTEGER NewVolume) const;
		void SetChannelVolume(LUA_INTEGER Channel, LUA_INTEGER NewVolume) const;

		void PrintLoadedResources() const;
		void PrintActorProperties(std::string ActorID) const;

		LUA_INTEGER AnimInfLoop() const;

	private:
		sel::State& State;
};
