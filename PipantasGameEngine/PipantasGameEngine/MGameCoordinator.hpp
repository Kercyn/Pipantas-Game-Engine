#pragma once

/*! \file */

#include <vector>
#include <string>
#include <cstdint>

#include "IManager.hpp"
#include "CCamera.hpp"
#include "AccessKeys.hpp"

enum class EGameState : std::uint8_t
{
	Initializing,
	Playing,
	Paused,
	Loading,
	Exit
};

/*! \brief Overseeing manager of the engine. Responsible for coordinating the other managers and the flow of the game.
*/
class MGameCoordinator final : public IManager
{
	public:
		MGameCoordinator() = default;
		~MGameCoordinator() = default;

		void Initialize(const KeyGameCoordinator& Key, const std::vector<std::string>& Args) override;
		void Shutdown(const KeyGameCoordinator& Key) override;

		//! The game loop.
		void Run(const std::vector<std::string>& Args);

		/*! \brief Changes the game state to indicate that the game should exit.
		*/
		void RequestExit();
		void RequestLoading(const KeySceneSwitcher&);
		void FinishedLoading(const KeySceneSwitcher&);

		void WindowLostFocus();
		void WindowGainedFocus();

		EGameState GetGameState() const;
		float GetDeltaTime() const;
		float GetFixedDeltaTime() const;

	private:
		EGameState GameState;

		//! Physics calculations timestep.
		float FixedDeltaTime;

		//! The time it took for the previous frame to complete.
		float DeltaTime;

		//! Timestep accumulator.
		float Accumulator;
};
