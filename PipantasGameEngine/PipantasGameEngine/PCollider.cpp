#include "PCollider.hpp"
#include "AActor.hpp"
#include "SCircle.hpp"
#include "SOBB.hpp"
#include "Utils.hpp"

#include <limits>

PCollider::PCollider()
	: Layer(nullptr), 
	Offset(glm::vec4()), 
	bIsStatic(false), 
	TriggerType(ETriggerType::None),
	bEnabledSelf(true)
{
}

PCollider::PCollider(const PCollider& other)
{
	bIsStatic = other.bIsStatic;
	TriggerType = other.TriggerType;
	bEnabledSelf = other.bEnabledSelf;

	Name = other.Name;
	Layer = other.Layer;
	Offset = other.Offset;
	
	Shape = other.Shape->Clone();
}

PCollider& PCollider::operator=(const PCollider& other)
{
	if (this != &other)
	{
		bIsStatic = other.bIsStatic;
		TriggerType = other.TriggerType;
		bEnabledSelf = other.bEnabledSelf;

		Name = other.Name;
		Layer = other.Layer;
		Offset = other.Offset;

		Shape = other.Shape->Clone();
	}

	return *this;
}

const std::vector<glm::vec2>& PCollider::GetVertices() const
{
	return Shape->GetVertices();
}

bool PCollider::IsStatic() const
{
	return bIsStatic;
}

bool PCollider::IsTrigger() const
{
	return (! (TriggerType == ETriggerType::None));
}

bool PCollider::IsEnabledSelf() const
{
	return bEnabledSelf;
}

bool PCollider::IsEnabledParent() const
{
	if (AttachedActor != nullptr)
	{
		return AttachedActor->IsEnabled();
	}
	else
	{
		return true;
	}
}

bool PCollider::IsActive() const
{
	return bEnabledSelf && IsEnabledParent();
}

const PCollisionLayer* const PCollider::GetLayer() const
{
	return Layer;
}

std::string PCollider::GetName() const
{
	return Name;
}

const SAABB& PCollider::GetHull() const
{
	return ConvexHull;
}

std::uint16_t PCollider::GetID() const
{
	return ID;
}

const AActor* const PCollider::GetAttachedActor() const
{
	return AttachedActor;
}

const glm::vec2& PCollider::GetOffset() const
{
	return Offset;
}

glm::vec2 PCollider::GetAbsolutePosition() const
{
	if (AttachedActor != nullptr)
	{
		return AttachedActor->GetBody()->GetPosition() + Offset;
	}
	else
	{
		return Offset;
	}
}

void PCollider::ApplyRotation()
{
	float ActorRotation = 0.0f;

	if (AttachedActor != nullptr)
	{
		auto ActorBody = AttachedActor->GetBody();
		switch (ActorBody->GetType())
		{
			case EShapeType::Circle:
			{
				const auto Circle = static_cast<const SCircle* const>(ActorBody);
				ActorRotation = Circle->GetRotation();
				break;
			}

			case EShapeType::OBB:
			{
				const auto OBB = static_cast<const SOBB* const>(ActorBody);
				ActorRotation = OBB->GetRotation();
				break;
			}
		}
	}

	switch (Shape->GetType())
	{
		case EShapeType::Circle:
		{
			SCircle* Circle = static_cast<SCircle*>(Shape.get());
			Circle->SetRotation(Circle->GetRotation() + ActorRotation);
			break;
		}

		case EShapeType::OBB:
		{
			SOBB* OBB = static_cast<SOBB*>(Shape.get());
			OBB->SetRotation(OBB->GetRotation() + ActorRotation);
			break;
		}
	}
}

SShape* PCollider::GetShape() const
{
	return Shape.get();
}

void PCollider::SetScale(glm::vec2 NewScale)
{
	if (Shape->GetScale() != NewScale)
	{
		bNeedsUpdating = true;
		Shape->SetScale(NewScale);
	}
}

void PCollider::SetOffset(glm::vec2 NewOffset)
{
	if (Offset != NewOffset)
	{
		bNeedsUpdating = true;
		Offset = NewOffset;
	}
}

void PCollider::Enable()
{
	bEnabledSelf = true;
}

void PCollider::Disable()
{
	bEnabledSelf = false;
}

void PCollider::NeedsUpdate()
{
	bNeedsUpdating = true;
}

void PCollider::Update()
{
	Shape->SetPosition(GetAbsolutePosition());
	ApplyRotation();
	UpdateHull();
	bNeedsUpdating = false;
}

void PCollider::UpdateHull()
{
	switch (Shape->GetType())
	{
		case EShapeType::AABB:
		{
			SAABB* ShapeAABB = static_cast<SAABB*>(Shape.get());
			ConvexHull = *ShapeAABB;
			break;
		}
		case EShapeType::Circle:
		{
			float Radius = static_cast<SCircle*>(Shape.get())->GetRadius();
			auto Min = glm::vec2(-Radius, -Radius);
			auto Max = glm::vec2(Radius, Radius);
			ConvexHull.SetBoundaries(Min, Max);
			break;
		}
		case EShapeType::OBB:
		{
			SOBB* OBB = static_cast<SOBB*>(Shape.get());

			glm::vec2 Min;
			glm::vec2 Max;

			Min.x = OBB->GetSupportPoint(glm::vec2(-1.0f, 0.0f)).x;
			Min.y = OBB->GetSupportPoint(glm::vec2(0.0f, -1.0f)).y;

			Max.x = OBB->GetSupportPoint(glm::vec2(1.0f, 0.0f)).x;
			Max.y = OBB->GetSupportPoint(glm::vec2(0.0f, 1.0f)).y;

			Min -= Shape->GetPosition();
			Max -= Shape->GetPosition();

			ConvexHull.SetBoundaries(Min, Max);

			break;
		}
	} // switch (Shape->GetType())

	ConvexHull.SetPosition(GetAbsolutePosition());
}
