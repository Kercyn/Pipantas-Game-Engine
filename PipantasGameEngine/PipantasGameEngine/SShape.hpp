#pragma once

/*! \file */

#include <memory>
#include <vector>

#include "PLine.hpp"
#include "glm\glm.hpp"

enum EShapeType
{
	OBB,
	Circle,
	AABB,
	TOTAL
};

class SShape
{
	public:
		SShape();
		virtual ~SShape() = 0
		{}

		/*! \brief Clones the object, creating and returning a new identical one.
		*/
		virtual std::unique_ptr<SShape> Clone() const = 0;

		/*! \brief Projects the shape into an axis.
		\param Axis The vector to project the shape into.
		\return The shape's projection line segment.
		*/
		virtual Line ProjectInto(const glm::vec2& Axis) const = 0;

		//! Returns true if Point is inside the shape, false otherwise.
		virtual bool IsPointInside(const glm::vec2& Point) const = 0;

		virtual glm::mat4 GetModelMatrix() const = 0;

		virtual glm::vec2 GetSize() const = 0;

		void SetPosition(const glm::vec2& NewPosition);
		void SetPosition(float NewX, float NewY);
		void Translate(float OffsetX, float OffsetY);
		void Translate(const glm::vec2& Offset);

		void SetScale(glm::vec2 NewScale);
		void SetScale(float x, float y);
		void ChangeScale(float NewX, float NewY);

		const std::vector<glm::vec2>& GetVertices() const;
		const std::vector<glm::vec2>& GetNormals() const;
		const glm::vec2& GetPosition() const;
		const glm::vec2& GetScale() const;
		const glm::mat2& GetScaleMat() const;
		float GetRotation() const;
		const glm::mat2& GetRotationMat() const;
		EShapeType GetType() const;

	protected:
		EShapeType Type;

		glm::mat4 TranslationMat;
		glm::mat2 ScaleMat;
		glm::mat2 RotationMat;

		glm::vec2 Position;
		glm::vec2 Scale;
		//! In radians.
		float Rotation;

		std::vector<glm::vec2> Vertices;
		std::vector<glm::vec2> Normals;

		void UpdateTranslationMat();
		void UpdateScaleMat();
		
		//! Updates the values of vertices and normals to mirror any changes to the shape.
		virtual void UpdateVerticesAndNormals() = 0;
};
