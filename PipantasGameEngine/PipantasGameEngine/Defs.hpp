#pragma once

/*! \file */

#include <string>

/*! \brief Includes various definitions about the game and the engine.
*/
namespace Pipantas
{
	namespace Defs
	{
		extern std::string GAME_TITLE;
		extern std::string GAME_VERSION;

		extern const std::string FILE_BINDINGS;
		extern const std::string FILE_SETTINGS;
		extern const std::string FILE_STDOUT;
		extern const std::string FILE_RESOURCES;
		extern const std::string FILE_LOG;
		extern const std::string INVALID_KEY;

		// Settings keys
		extern const std::string SCREEN_WIDTH;
		extern const std::string SCREEN_HEIGHT;
		extern const std::string SCREEN_BPP;
		extern const std::string SCREEN_FULLSCREEN;
	}
}