#include <cstring>
#include <string>
#include <gl/glew.h>

#include "MRenderer.hpp"
#include "Managers.hpp"
#include "MapElements.hpp"
#include "SDL.h"
#include "SDL_image.h"
#include "Utils.hpp"
#include "Defs.hpp"
#include "CCamera.hpp"
#include "rapidxml.hpp"
#include "Utils.hpp"
#include "Pipanikos.hpp"
#include "ErrorHandling.hpp"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

MRenderer::MRenderer()
	: SDLWindow(nullptr, SDL_DestroyWindow), SDLRenderer(nullptr, SDL_DestroyRenderer)
{
}

void MRenderer::Initialize(const KeyGameCoordinator&, const std::vector<std::string>&)
{
	using namespace rapidxml;
	std::stringstream Message;

	FileParser.Open(Pipantas::Defs::FILE_SETTINGS);

	Resolution.x = FileParser.GetValue(Pipantas::Defs::SCREEN_WIDTH);
	Resolution.y = FileParser.GetValue(Pipantas::Defs::SCREEN_HEIGHT);
	BPP = FileParser.GetValue(Pipantas::Defs::SCREEN_BPP);

	WindowFlags = SDL_INIT_TIMER | SDL_INIT_VIDEO;
	ScreenFlags = SDL_WINDOW_OPENGL;
	IMGInitFlags = IMG_INIT_PNG;

	int FullscreenType = FileParser.GetValue(Pipantas::Defs::SCREEN_FULLSCREEN);

	switch (FullscreenType)
	{
		case 0:
			// No extra flags needed
			break;
		case 1:
			ScreenFlags |= SDL_WINDOW_FULLSCREEN;
			break;
		case 2:
			ScreenFlags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
			break;
	}

	if (SDL_Init(WindowFlags) != 0)
	{
		std::stringstream ErrorMessage;
		ErrorMessage << SDL_GetError() << " Flags: 0x" << std::hex << WindowFlags;
		throw(Pipanikos(ErrorMessage.str(), __FILE__, __LINE__));
	}
	Message << "Initialized SDL with window flags: 0x" << std::hex << WindowFlags;
	Logger.Log(ELogLevel::Trace, Message.str());

	if (IMG_Init(IMGInitFlags) != IMGInitFlags)
	{
		std::stringstream ErrorMessage;
		ErrorMessage << IMG_GetError << " Flags: 0x" << std::hex << IMGInitFlags;
		throw(Pipanikos(ErrorMessage.str(), __FILE__, __LINE__));
	}
	Message.clear();
	Message.str("");
	Message << "Initialized SDL_image with flags: 0x" << std::hex << IMGInitFlags;
	Logger.Log(ELogLevel::Trace, Message.str());

	SDLWindow.reset(SDL_CreateWindow(std::string(Pipantas::Defs::GAME_TITLE + " v" + Pipantas::Defs::GAME_VERSION).c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, Resolution.x, Resolution.y, ScreenFlags));
	if (SDLWindow == nullptr)
	{
		std::stringstream ErrorMessage;
		ErrorMessage << "Unable to create window with parameters: " << std::endl;
		ErrorMessage << "Resolution: " <<  Resolution.x << "x" << Resolution.y << std::endl;
		ErrorMessage << "Bits per pixel: " << BPP << std::endl;
		ErrorMessage << "Flags: 0x" << std::hex << ScreenFlags;
		throw(Pipanikos(ErrorMessage.str(), __FILE__, __LINE__));
	}

	Message.clear();
	Message.str("");
	Message << "Created window with parameters: " << std::endl;
	Message << "\tResolution: " << std::dec << Resolution.x << "x" << Resolution.y << std::endl;
	Message << "\tBits per pixel: " << BPP << std::endl;
	Message << "\tFlags: 0x" << std::hex << ScreenFlags;
	Logger.Log(ELogLevel::Trace, Message.str());

	SDLRenderer.reset(SDL_CreateRenderer(SDLWindow.get(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC));
	if (SDLRenderer == nullptr)
	{
		throw(Pipanikos(SDL_GetError(), __FILE__, __LINE__));
	}

	if (SDL_SetRenderDrawColor(SDLRenderer.get(), 0u, 0u, 0u, SDL_ALPHA_OPAQUE) < 0)
	{
		throw(Pipanikos(SDL_GetError(), __FILE__, __LINE__));
	}

	if (SDL_SetRenderDrawBlendMode(SDLRenderer.get(), SDL_BLENDMODE_BLEND) < 0)
	{
		throw(Pipanikos(SDL_GetError(), __FILE__, __LINE__));
	}

	if (SDL_RenderClear(SDLRenderer.get()) < 0)
	{
		throw(Pipanikos(SDL_GetError(), __FILE__, __LINE__));
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

	GLContext = SDL_GL_CreateContext(SDLWindow.get());

	// Initialize OpenGL-related stuff
	GL_CALL(SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8));
	GL_CALL(SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8));
	GL_CALL(SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8));
	GL_CALL(SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8));

	GL_CALL(SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, BPP));
	GL_CALL(SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32));

	GL_CALL(SDL_GL_SetAttribute(SDL_GL_ACCUM_RED_SIZE, 8));
	GL_CALL(SDL_GL_SetAttribute(SDL_GL_ACCUM_GREEN_SIZE, 8));
	GL_CALL(SDL_GL_SetAttribute(SDL_GL_ACCUM_BLUE_SIZE, 8));
	GL_CALL(SDL_GL_SetAttribute(SDL_GL_ACCUM_ALPHA_SIZE, 8));

	GL_CALL(SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1));
	GL_CALL(SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 2));

	GL_CALL(glEnable(GL_BLEND));
	GL_CALL(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

	if (Resolution.x > Resolution.y)
	{
		GL_CALL(glViewport(0, (Resolution.y - Resolution.x) / 2, Resolution.x, Resolution.x));
	}
	else
	{
		GL_CALL(glViewport((Resolution.x - Resolution.y) / 2, 0, Resolution.y, Resolution.y));
	}

	SDL_SetRelativeMouseMode(SDL_FALSE);

	glewInit();

	// Generate buffers

	GL_CALL(glGenBuffers(1, &UBO));
	glBindBuffer(GL_UNIFORM_BUFFER, UBO);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(UBOData), &UBOData, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	RenderDataMap.emplace(ResourceStore.Keys.RENDER_DATA_STATIC_COLLIDER, RenderData());
	RenderDataMap.emplace(ResourceStore.Keys.RENDER_DATA_DYNAMIC_COLLIDER, RenderData());
	RenderDataMap.emplace(ResourceStore.Keys.RENDER_DATA_MAP_BACK, RenderData());
	RenderDataMap.emplace(ResourceStore.Keys.RENDER_DATA_MAP_FRONT, RenderData());
	RenderDataMap.emplace(ResourceStore.Keys.RENDER_DATA_SAP, RenderData());

	Logger.Log(ELogLevel::Info, "MRenderer initialized.");
}

void MRenderer::Shutdown(const KeyGameCoordinator&)
{
	if (SDL_WasInit(WindowFlags) == WindowFlags)
	{
		IMG_Quit();
		SDL_Quit();
		Logger.Log(ELogLevel::Info, "MRenderer shutdown.");
	}
	else
	{
		Logger.Log(ELogLevel::Warning, "SDL has not been initialized.");
	}
}

void MRenderer::Reset()
{
	Camera.SetPosition(glm::vec2(0.0f, 0.0f));

	for (auto& KVP : ActorRenderData)
	{
		auto& RenderData = KVP.second;

		GL_CALL(glBindVertexArray(0));
		GL_CALL(glDeleteVertexArrays(1, &RenderData.VAO));

		GL_CALL(glBindBuffer(GL_ARRAY_BUFFER , 0));
		GL_CALL(glDeleteBuffers(1, &RenderData.VBO));
	}
	ActorRenderData.clear();

	for (auto& KVP : ParallaxRenderData)
	{
		auto& RenderData = KVP.second;

		GL_CALL(glBindVertexArray(0));
		GL_CALL(glDeleteVertexArrays(1, &RenderData.VAO));

		GL_CALL(glBindBuffer(GL_ARRAY_BUFFER , 0));
		GL_CALL(glDeleteBuffers(1, &RenderData.VBO));
		GL_CALL(glDeleteBuffers(1, &RenderData.InstanceVBO));
	}
	ParallaxRenderData.clear();

	for (auto& KVP : RenderDataMap)
	{
		auto& RenderData = KVP.second;

		GL_CALL(glBindVertexArray(0));
		GL_CALL(glDeleteVertexArrays(1, &RenderData.VAO));

		GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, 0));
		GL_CALL(glDeleteBuffers(1, &RenderData.VBO));
		GL_CALL(glDeleteBuffers(1, &RenderData.InstanceVBO));

		RenderData.ClearContainers();
	}

	GenerateRenderDataMapBuffers();
}

void MRenderer::GenerateNewMapRenderData()
{
	GenerateMapRenderData();
	GenerateParallaxRenderData();
	GenerateSAPRenderData();
	GenerateStaticColliderRenderData();
}

void MRenderer::InitializeActorRenderData(std::string ID)
{
	ActorRenderData.emplace(ID, RenderData());
	auto& RenderData = ActorRenderData.at(ID);

	GL_CALL(glGenBuffers(1, &RenderData.VBO));
	GL_CALL(glGenVertexArrays(1, &RenderData.VAO));

	RenderData.VertexCount.resize(1);
	RenderData.VertexIndex.resize(1);
	RenderData.ModelMatrix.resize(1);
}

void MRenderer::InitializeParallaxRenderData()
{
	const auto& ParallaxGroup = ParallaxStore.GetCurrentGroup();

	for (const auto& KVP : ParallaxGroup)
	{
		std::string Key = KVP.first;

		ParallaxRenderData[Key] = RenderData();
		auto& RenderData = ParallaxRenderData.at(Key);

		GL_CALL(glGenBuffers(1, &RenderData.VBO));
		GL_CALL(glGenBuffers(1, &RenderData.InstanceVBO));
		GL_CALL(glGenVertexArrays(1, &RenderData.VAO));
	}
}

void MRenderer::Render()
{
	GL_CALL(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
	GL_CALL(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

	if (GameCoordinator.GetGameState() == EGameState::Playing)
	{
		UpdateShaderUniforms();
		UpdateParallaxInstanceVBO();
		UpdateParallaxModelMatrix();

		static const auto& MapShader = ResourceStore.RequestShaderProgram(ResourceStore.Keys.SHADER_PROG_MAP);

		static GLint SharedInfoLoc = glGetUniformBlockIndex(MapShader, "shared_data");
		GL_CALL(glBindBufferBase(GL_UNIFORM_BUFFER, 0, UBO));
		GL_CALL(glUniformBlockBinding(MapShader, SharedInfoLoc, 0));

		RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_DYNAMIC_COLLIDER).ClearContainers();
		GenerateActorRenderData();

		RenderBackParallax();
		RenderBackMapLayers();
		RenderActors();
		RenderFrontMapLayers();
		RenderFrontParallax();

		if (bDebugPhysics)
		{
			GenerateDynamicColliderRenderData();
			RenderColliders();
			RenderSAP();
		}
	}

	SDL_GL_SwapWindow(SDLWindow.get());
}

void MRenderer::SetDebugPhysics(bool Flag)
{
	bDebugPhysics = Flag;
}

void MRenderer::GenerateMapRenderData()
{
	const auto Map = SceneSwitcher.GetCurrentMap();

	std::vector<GLfloat> FrontVertexBuffer;
	std::vector<GLfloat> BackVertexBuffer;
	
	// The position of the current tile's upper-left vertex in map coordinates.
	SDL_Point TilePosition;

	// The map's tile size.
	SDL_Point TileSize;

	// Tile size to Tileset size ratio, I'm not even sorry for the name
	const glm::vec2 TTTR = Map->Tileset.TileToTilesetRatio;;

	TileSize.x = Map->Map.TileWidth;
	TileSize.y = Map->Map.TileHeight;

	int TotalLines = Map->GetHeight();
	int TotalColumns = Map->GetWidth();

	int FrontIndex = 0;
	int BackIndex = 0;

	for (const auto& Layer : Map->Layers)
	{
		TilePosition.x = 0;
		TilePosition.y = 0;

		if (Layer.bIsVisible)
		{
			for (int Line = TotalLines - 1; Line >= 0; Line--)
			{
				TilePosition.x = 0;

				for (int Column = 0; Column < TotalColumns; Column++)
				{
					MapElements::Tile Tile = Layer.Tiles[Map->Map.Width * Line + Column];

					if (Tile.GID != 0)
					{
						if (auto RenderInFrontVal = Layer.Properties.GetProperty("RenderInFront"))
						{
							bool bRenderInFront = Utils::ToBool(*RenderInFrontVal);

							if (bRenderInFront)
							{
								// SDL loads the texture upside-down, so we must do some special magic when applying texture coords.
								// Top left
								FrontVertexBuffer.emplace_back(TilePosition.x);
								FrontVertexBuffer.emplace_back(TilePosition.y);
								FrontVertexBuffer.emplace_back(Tile.Offset.x);
								FrontVertexBuffer.emplace_back(Tile.Offset.y + TTTR.y);
								FrontVertexBuffer.emplace_back(Layer.Opacity);

								// Top right
								FrontVertexBuffer.emplace_back(TilePosition.x + TileSize.x);
								FrontVertexBuffer.emplace_back(TilePosition.y);
								FrontVertexBuffer.emplace_back(Tile.Offset.x + TTTR.x);
								FrontVertexBuffer.emplace_back(Tile.Offset.y + TTTR.y);
								FrontVertexBuffer.emplace_back(Layer.Opacity);

								// Bottom right
								FrontVertexBuffer.emplace_back(TilePosition.x + TileSize.x);
								FrontVertexBuffer.emplace_back(TilePosition.y + TileSize.y);
								FrontVertexBuffer.emplace_back(Tile.Offset.x + TTTR.x);
								FrontVertexBuffer.emplace_back(Tile.Offset.y);
								FrontVertexBuffer.emplace_back(Layer.Opacity);

								// Bottom left
								FrontVertexBuffer.emplace_back(TilePosition.x);
								FrontVertexBuffer.emplace_back(TilePosition.y + TileSize.y);
								FrontVertexBuffer.emplace_back(Tile.Offset.x);
								FrontVertexBuffer.emplace_back(Tile.Offset.y);
								FrontVertexBuffer.emplace_back(Layer.Opacity);

								RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_MAP_FRONT).VertexCount.emplace_back(4);
								RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_MAP_FRONT).VertexIndex.emplace_back(FrontIndex);
								FrontIndex += 4;
							}
							else
							{
								// Top left
								BackVertexBuffer.emplace_back(TilePosition.x);
								BackVertexBuffer.emplace_back(TilePosition.y);
								BackVertexBuffer.emplace_back(Tile.Offset.x);
								BackVertexBuffer.emplace_back(Tile.Offset.y + TTTR.y);
								BackVertexBuffer.emplace_back(Layer.Opacity);

								// Top right
								BackVertexBuffer.emplace_back(TilePosition.x + TileSize.x);
								BackVertexBuffer.emplace_back(TilePosition.y);
								BackVertexBuffer.emplace_back(Tile.Offset.x + TTTR.x);
								BackVertexBuffer.emplace_back(Tile.Offset.y + TTTR.y);
								BackVertexBuffer.emplace_back(Layer.Opacity);

								// Bottom right
								BackVertexBuffer.emplace_back(TilePosition.x + TileSize.x);
								BackVertexBuffer.emplace_back(TilePosition.y + TileSize.y);
								BackVertexBuffer.emplace_back(Tile.Offset.x + TTTR.x);
								BackVertexBuffer.emplace_back(Tile.Offset.y);
								BackVertexBuffer.emplace_back(Layer.Opacity);

								// Bottom left
								BackVertexBuffer.emplace_back(TilePosition.x);
								BackVertexBuffer.emplace_back(TilePosition.y + TileSize.y);
								BackVertexBuffer.emplace_back(Tile.Offset.x);
								BackVertexBuffer.emplace_back(Tile.Offset.y);
								BackVertexBuffer.emplace_back(Layer.Opacity);

								RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_MAP_BACK).VertexCount.emplace_back(4);
								RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_MAP_BACK).VertexIndex.emplace_back(BackIndex);
								BackIndex += 4;
							}
						}
						else
						{
							throw(Pipanikos("RenderInFront property not found in layer " + Layer.Name + " in map " + Map->ID, __FILE__, __LINE__));
						}
					} // if (Tile.GID != 0)

					TilePosition.x += TileSize.x;
				} // Columns loop

				TilePosition.y += TileSize.y;
			} // Lines loop
		} // if (Layer.bIsVisible)
	} // for (const auto& Layer : Map->Layers)

	GL_CALL(glBindVertexArray(RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_MAP_FRONT).VAO));
	GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_MAP_FRONT).VBO));
	GL_CALL(glBufferData(GL_ARRAY_BUFFER, FrontVertexBuffer.size() * sizeof(GLfloat), FrontVertexBuffer.data(), GL_STATIC_DRAW));

	const auto& MapShader = ResourceStore.RequestShaderProgram(ResourceStore.Keys.SHADER_PROG_MAP);

	GL_CALL(glEnableVertexAttribArray(MapShader.GetAttribLoc("vert")));
	GL_CALL(glVertexAttribPointer(MapShader.GetAttribLoc("vert"), 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5, nullptr));

	GL_CALL(glEnableVertexAttribArray(MapShader.GetAttribLoc("vertTexCoord")));
	GL_CALL(glVertexAttribPointer(MapShader.GetAttribLoc("vertTexCoord"), 2, GL_FLOAT, GL_TRUE, sizeof(GLfloat) * 5, (const GLvoid*)(2 * sizeof(GLfloat))));

	GL_CALL(glEnableVertexAttribArray(MapShader.GetAttribLoc("alpha")));
	GL_CALL(glVertexAttribPointer(MapShader.GetAttribLoc("alpha"), 1, GL_FLOAT, GL_TRUE, sizeof(GLfloat) * 5, (const GLvoid*)(4 * sizeof(GLfloat))));

	GL_CALL(glBindVertexArray(RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_MAP_BACK).VAO));
	GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_MAP_BACK).VBO));
	GL_CALL(glBufferData(GL_ARRAY_BUFFER, BackVertexBuffer.size() * sizeof(GLfloat), BackVertexBuffer.data(), GL_STATIC_DRAW));

	GL_CALL(glEnableVertexAttribArray(MapShader.GetAttribLoc("vert")));
	GL_CALL(glVertexAttribPointer(MapShader.GetAttribLoc("vert"), 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5, nullptr));

	GL_CALL(glEnableVertexAttribArray(MapShader.GetAttribLoc("vertTexCoord")));
	GL_CALL(glVertexAttribPointer(MapShader.GetAttribLoc("vertTexCoord"), 2, GL_FLOAT, GL_TRUE, sizeof(GLfloat) * 5, (const GLvoid*)(2 * sizeof(GLfloat))));

	GL_CALL(glEnableVertexAttribArray(MapShader.GetAttribLoc("alpha")));
	GL_CALL(glVertexAttribPointer(MapShader.GetAttribLoc("alpha"), 1, GL_FLOAT, GL_TRUE, sizeof(GLfloat) * 5, (const GLvoid*)(4 * sizeof(GLfloat))));

	GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, 0));
	GL_CALL(glBindVertexArray(0));
}

void MRenderer::GenerateParallaxRenderData()
{
	static const auto& ParallaxShader = ResourceStore.RequestShaderProgram(ResourceStore.Keys.SHADER_PROG_PARALLAX);

	ParallaxShader.Use();

	auto& ParallaxGroup = ParallaxStore.GetCurrentGroup();

	for (const auto& KVP : ParallaxGroup)
	{
		const std::string Key = KVP.first;
		const auto& Parallax = KVP.second;

		SDL_Point ParallaxSize;
		SAABB ParallaxBox;
		std::vector<GLfloat> VertexBuffer;

		ParallaxSize.x = ResourceStore.RequestTexture(Key).Width;
		ParallaxSize.y = ResourceStore.RequestTexture(Key).Height;

		ParallaxBox.SetMin(-(ParallaxSize.x / 2.0f), -(ParallaxSize.y / 2.0f));
		ParallaxBox.SetMax(ParallaxSize.x / 2.0f, ParallaxSize.y / 2.0f);

		const auto& ParMin = ParallaxBox.GetMin();
		const auto& ParMax = ParallaxBox.GetMax();
			
		VertexBuffer.emplace_back(ParMin.x);
		VertexBuffer.emplace_back(ParMax.y);
		VertexBuffer.emplace_back(0.0f);
		VertexBuffer.emplace_back(0.0f);

		VertexBuffer.emplace_back(ParMin.x);
		VertexBuffer.emplace_back(ParMin.y);
		VertexBuffer.emplace_back(0.0f);
		VertexBuffer.emplace_back(1.0f);

		VertexBuffer.emplace_back(ParMax.x);
		VertexBuffer.emplace_back(ParMin.y);
		VertexBuffer.emplace_back(1.0f);
		VertexBuffer.emplace_back(1.0f);

		VertexBuffer.emplace_back(ParMax.x);
		VertexBuffer.emplace_back(ParMax.y);
		VertexBuffer.emplace_back(1.0f);
		VertexBuffer.emplace_back(0.0f);

		ParallaxRenderData.emplace(Key, RenderData());
		auto& RenderData = ParallaxRenderData.at(Key);
		RenderData.VertexCount.emplace_back(4);
		RenderData.VertexIndex.emplace_back(0);
		RenderData.ModelMatrix.emplace_back(glm::mat4(1.0f));

		GL_CALL(glBindVertexArray(RenderData.VAO));

		GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, RenderData.VBO));
		GL_CALL(glBufferData(GL_ARRAY_BUFFER, VertexBuffer.size() * sizeof(GLfloat), VertexBuffer.data(), GL_STATIC_DRAW));

		GL_CALL(glEnableVertexAttribArray(ParallaxShader.GetAttribLoc("vert")));
		GL_CALL(glVertexAttribPointer(ParallaxShader.GetAttribLoc("vert"), 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 4, nullptr));

		GL_CALL(glEnableVertexAttribArray(ParallaxShader.GetAttribLoc("vertTexCoord")));
		GL_CALL(glVertexAttribPointer(ParallaxShader.GetAttribLoc("vertTexCoord"), 2, GL_FLOAT, GL_TRUE, sizeof(GLfloat) * 4, (const GLvoid*)(2 * sizeof(GLfloat))));

		GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, 0));
		GL_CALL(glBindVertexArray(0));

		ParallaxShader.StopUsing();
	}
}

void MRenderer::GenerateSAPRenderData()
{
	std::vector<GLfloat> VertexBuffer;
	int Index = 0;
	std::vector<GLint> SAPIndices;
	std::vector<GLsizei> SAPVertexCount;

	for (const auto& SAP : PairGenerator.GetSAPs())
	{
		auto Boundary = SAP.GetBoundaries();

		VertexBuffer.emplace_back(Boundary.GetMin().x);
		VertexBuffer.emplace_back(Boundary.GetMin().y);
		VertexBuffer.emplace_back(1.0f);

		VertexBuffer.emplace_back(Boundary.GetMin().x);
		VertexBuffer.emplace_back(Boundary.GetMax().y);
		VertexBuffer.emplace_back(1.0f);

		VertexBuffer.emplace_back(Boundary.GetMax().x);
		VertexBuffer.emplace_back(Boundary.GetMax().y);
		VertexBuffer.emplace_back(1.0f);

		VertexBuffer.emplace_back(Boundary.GetMax().x);
		VertexBuffer.emplace_back(Boundary.GetMin().y);
		VertexBuffer.emplace_back(1.0f);

		SAPIndices.emplace_back(Index);
		SAPVertexCount.emplace_back(4);
		RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_SAP).ModelMatrix.emplace_back(SAP.GetBoundaries().GetModelMatrix());

		Index += 4;
	}

	RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_SAP).VertexCount = SAPVertexCount;
	RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_SAP).VertexIndex = SAPIndices;
	
	GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_SAP).VBO));
	GL_CALL(glBufferData(GL_ARRAY_BUFFER, VertexBuffer.size() * sizeof(GLfloat), VertexBuffer.data(), GL_STATIC_DRAW));
}

Uint32 MRenderer::GetWindowFlags() const
{
	return SDL_GetWindowFlags(SDLWindow.get());
}

Uint32 MRenderer::GetWindowID() const
{
	return SDL_GetWindowID(SDLWindow.get());
}

CCamera& MRenderer::GetCamera()
{
	return Camera;
}

void MRenderer::GenerateActorRenderData()
{
	static const auto& ActorShader = ResourceStore.RequestShaderProgram(ResourceStore.Keys.SHADER_PROG_ACTOR);

	ActorShader.Use();

	for (const auto Actor : ActorHandler.GetAllActors())
	{
		if (Actor->GetActorType() != EActorType::Audio)
		{
			TextureCoords TexCoords;

			if (Actor->GetActorType() == EActorType::Character)
			{
				auto Character = static_cast<ACharacter* const>(Actor);
				TexCoords = Character->GetTexCoords();
			}
			else
			{
				TexCoords = Actor->GetTexCoords();
			}

			const auto& ActorVertices = Actor->GetBody()->GetVertices();
			auto ActorID = Actor->GetID();

			std::vector<GLfloat> VertexBuffer;

			switch (Actor->GetBody()->GetType())
			{
				case EShapeType::OBB:
				{
					VertexBuffer.emplace_back(ActorVertices.at(0).x);
					VertexBuffer.emplace_back(ActorVertices.at(0).y);
					VertexBuffer.emplace_back(TexCoords.Start.x);
					VertexBuffer.emplace_back(TexCoords.End.y);

					VertexBuffer.emplace_back(ActorVertices.at(1).x);
					VertexBuffer.emplace_back(ActorVertices.at(1).y);
					VertexBuffer.emplace_back(TexCoords.Start.x);
					VertexBuffer.emplace_back(TexCoords.Start.y);

					VertexBuffer.emplace_back(ActorVertices.at(2).x);
					VertexBuffer.emplace_back(ActorVertices.at(2).y);
					VertexBuffer.emplace_back(TexCoords.End.x);
					VertexBuffer.emplace_back(TexCoords.Start.y);

					VertexBuffer.emplace_back(ActorVertices.at(3).x);
					VertexBuffer.emplace_back(ActorVertices.at(3).y);
					VertexBuffer.emplace_back(TexCoords.End.x);
					VertexBuffer.emplace_back(TexCoords.End.y);

					break;
				}

				case EShapeType::Circle:
				{
					const SCircle* Circle = static_cast<const SCircle*>(Actor->GetBody());
					glm::vec2 Center = Circle->GetPosition();
					float Radius = Circle->GetRadius();

					for (const auto& Vertex : ActorVertices)
					{
						VertexBuffer.emplace_back(Vertex.x);
						VertexBuffer.emplace_back(Vertex.y);
						VertexBuffer.emplace_back(0.5f + Vertex.x  / (2 * Radius));
						VertexBuffer.emplace_back(0.5f + Vertex.y / (2 * Radius));
					}

					break;
				}
			}

			auto& RenderData = ActorRenderData.at(ActorID);
			RenderData.VertexCount[0] = ActorVertices.size();
			RenderData.VertexIndex[0] = 0;
			RenderData.ModelMatrix[0] = Actor->GetBody()->GetModelMatrix();

			GL_CALL(glBindVertexArray(RenderData.VAO));
			GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, RenderData.VBO));
			GL_CALL(glBufferData(GL_ARRAY_BUFFER, VertexBuffer.size() * sizeof(GLfloat), VertexBuffer.data(), GL_STATIC_DRAW));

			GL_CALL(glEnableVertexAttribArray(ActorShader.GetAttribLoc("vert")));
			GL_CALL(glVertexAttribPointer(ActorShader.GetAttribLoc("vert"), 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 4, nullptr));

			GL_CALL(glEnableVertexAttribArray(ActorShader.GetAttribLoc("vertTexCoord")));
			GL_CALL(glVertexAttribPointer(ActorShader.GetAttribLoc("vertTexCoord"), 2, GL_FLOAT, GL_TRUE, sizeof(GLfloat) * 4, (const GLvoid*)(2 * sizeof(GLfloat))));
		}
	}

	ActorShader.StopUsing();
}

void MRenderer::GenerateDynamicColliderRenderData()
{
	std::vector<GLfloat> VertexBuffer;

	// Current collider index.
	int Index = 0;

	auto& RenderData = RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_DYNAMIC_COLLIDER);
	RenderData.ClearContainers();

	for (const auto& KVP : CollisionEngine.GetColliders())
	{
		const PCollider* const Collider = KVP.second;

		if (! Collider->IsStatic())
		{
			auto Vertices = Collider->GetVertices();

			for (auto Vertex : Vertices)
			{
				VertexBuffer.emplace_back(Vertex.x);
				VertexBuffer.emplace_back(Vertex.y);

				float ColliderAlpha = (Collider->IsEnabledSelf()) ? 1.0f : 0.01f;
				VertexBuffer.emplace_back(ColliderAlpha);
			}

			RenderData.VertexCount.emplace_back(Vertices.size());
			RenderData.VertexIndex.emplace_back(Index);
			RenderData.ModelMatrix.emplace_back(Collider->GetShape()->GetModelMatrix());

			Index += Vertices.size();
		}

	}

	GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_DYNAMIC_COLLIDER).VBO));
	GL_CALL(glBufferData(GL_ARRAY_BUFFER, VertexBuffer.size() * sizeof(GLfloat), VertexBuffer.data(), GL_STATIC_DRAW));
}

void MRenderer::GenerateStaticColliderRenderData()
{
	std::vector<GLfloat> VertexBuffer;

	// Current collider index.
	int Index = 0;

	for (const auto& KVP : CollisionEngine.GetColliders())
	{
		const PCollider* const Collider = KVP.second;
		if (Collider->IsStatic())
		{
			auto Vertices = Collider->GetVertices();

			for (const auto& Vertex : Vertices)
			{
				VertexBuffer.emplace_back(Vertex.x);
				VertexBuffer.emplace_back(Vertex.y);

				float ColliderAlpha = (Collider->IsEnabledSelf()) ? 1.0f : 0.01f;
				VertexBuffer.emplace_back(ColliderAlpha);
			}

			RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_STATIC_COLLIDER).VertexCount.emplace_back(Vertices.size());
			RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_STATIC_COLLIDER).VertexIndex.emplace_back(Index);
			RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_STATIC_COLLIDER).ModelMatrix.emplace_back(Collider->GetShape()->GetModelMatrix());

			Index += Vertices.size();
		}

	}

	GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_STATIC_COLLIDER).VBO));
	GL_CALL(glBufferData(GL_ARRAY_BUFFER, VertexBuffer.size() * sizeof(GLfloat), VertexBuffer.data(), GL_STATIC_DRAW));
}

void MRenderer::GenerateRenderDataMapBuffers()
{
	GL_CALL(glGenBuffers(1, &RenderDataMap[ResourceStore.Keys.RENDER_DATA_STATIC_COLLIDER].VBO));
	GL_CALL(glGenVertexArrays(1, &RenderDataMap[ResourceStore.Keys.RENDER_DATA_STATIC_COLLIDER].VAO));

	GL_CALL(glGenBuffers(1, &RenderDataMap[ResourceStore.Keys.RENDER_DATA_DYNAMIC_COLLIDER].VBO));
	GL_CALL(glGenVertexArrays(1, &RenderDataMap[ResourceStore.Keys.RENDER_DATA_DYNAMIC_COLLIDER].VAO));

	GL_CALL(glGenBuffers(1, &RenderDataMap[ResourceStore.Keys.RENDER_DATA_MAP_FRONT].VBO));
	GL_CALL(glGenVertexArrays(1, &RenderDataMap[ResourceStore.Keys.RENDER_DATA_MAP_FRONT].VAO));

	GL_CALL(glGenBuffers(1, &RenderDataMap[ResourceStore.Keys.RENDER_DATA_MAP_BACK].VBO));
	GL_CALL(glGenVertexArrays(1, &RenderDataMap[ResourceStore.Keys.RENDER_DATA_MAP_BACK].VAO));

	GL_CALL(glGenBuffers(1, &RenderDataMap[ResourceStore.Keys.RENDER_DATA_SAP].VBO));
}

void MRenderer::UpdateShaderUniforms()
{
	auto CamPos = Camera.Matrix();

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			UBOData.CameraPosition[i][j] = CamPos[i][j];
		}
	}

	GL_CALL(glBindBuffer(GL_UNIFORM_BUFFER, UBO));
	GLvoid* BufferPtr = glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY);
	std::memcpy(BufferPtr, &UBOData, sizeof(UBOData));
	GL_CALL(glUnmapBuffer(GL_UNIFORM_BUFFER));
}

void MRenderer::UpdateParallaxInstanceVBO()
{
	static std::vector<glm::vec2> Offsets;
	static const auto& ParallaxShader = ResourceStore.RequestShaderProgram(ResourceStore.Keys.SHADER_PROG_PARALLAX);
	static const auto OffsetsLoc = ParallaxShader.GetAttribLoc("offset");

	const auto& ParallaxGroup = ParallaxStore.GetCurrentGroup();

	for (const auto& KVP : ParallaxGroup)
	{
		const std::string Key = KVP.first;
		const auto& Parallax = KVP.second;

		if (Parallax.bIsEnabled)
		{
			Offsets.clear();

			auto& RenderData = ParallaxRenderData.at(Key);
			const BTexture& Texture = ResourceStore.RequestTexture(Key);
			glm::vec2 Size;
			Size.x = Texture.Width;
			Size.y = Texture.Height;

			GL_CALL(glBindVertexArray(RenderData.VAO));
			GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, RenderData.InstanceVBO));

			switch (Parallax.Type)
			{
			case EParallaxType::Bi_X:
				Offsets.emplace_back(Parallax.Offset - glm::vec2(Size.x, 0.0f));
				Offsets.emplace_back(Parallax.Offset);
				Offsets.emplace_back(Parallax.Offset + glm::vec2(Size.x, 0.0f));
				break;

			case EParallaxType::Bi_Y:
				Offsets.emplace_back(Parallax.Offset - glm::vec2(0.0f, Size.y));
				Offsets.emplace_back(Parallax.Offset);
				Offsets.emplace_back(Parallax.Offset + glm::vec2(0.0f, Size.y));
				break;

			case EParallaxType::Multi:
				// First row
				Offsets.emplace_back(Parallax.Offset + glm::vec2(-Size.x, -Size.y));
				Offsets.emplace_back(Parallax.Offset + glm::vec2(0.0f, -Size.y));
				Offsets.emplace_back(Parallax.Offset + glm::vec2(Size.x, -Size.y));

				// Second row
				Offsets.emplace_back(Parallax.Offset + glm::vec2(-Size.x, 0.0f));
				Offsets.emplace_back(Parallax.Offset);
				Offsets.emplace_back(Parallax.Offset + glm::vec2(Size.x, 0.0f));

				// Third row
				Offsets.emplace_back(Parallax.Offset + glm::vec2(-Size.x, Size.y));
				Offsets.emplace_back(Parallax.Offset + glm::vec2(0.0f, Size.y));
				Offsets.emplace_back(Parallax.Offset + glm::vec2(Size.x, Size.y));
				break;

			case EParallaxType::Fixed:
				Offsets.emplace_back(Parallax.Offset);
				break;
			}

			RenderData.Instances = Offsets.size();

			GL_CALL(glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * Offsets.size(), Offsets.data(), GL_STREAM_DRAW));

			GL_CALL(glEnableVertexAttribArray(OffsetsLoc));
			GL_CALL(glVertexAttribPointer(OffsetsLoc, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), static_cast<GLvoid*>(0)));
			GL_CALL(glVertexAttribDivisor(OffsetsLoc, 1));

			GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, 0));
			GL_CALL(glBindVertexArray(0));
		}
	}
}

void MRenderer::UpdateParallaxModelMatrix()
{
	const auto& ParallaxGroup = ParallaxStore.GetCurrentGroup();

	for (const auto& KVP : ParallaxGroup)
	{
		auto& RenderData = ParallaxRenderData.at(KVP.first);
		const auto& CameraPos = Camera.GetPosition();

		RenderData.ModelMatrix[0] = glm::translate(glm::mat4(1.0f), glm::vec3(CameraPos.x, CameraPos.y, 0.0f));
	}
}

void MRenderer::RenderFrontMapLayers() const
{
	const BTexture& TilesetTexture = ResourceStore.RequestTexture(SceneSwitcher.GetCurrentMap()->Tileset.Name);
	static const auto& MapShader = ResourceStore.RequestShaderProgram(ResourceStore.Keys.SHADER_PROG_MAP);

	auto& RenderData = RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_MAP_FRONT);

	MapShader.Use();
	GL_CALL(glBindVertexArray(RenderData.VAO));

	GL_CALL(glBindTexture(GL_TEXTURE_2D, TilesetTexture));

	GL_CALL(glMultiDrawArrays(GL_TRIANGLE_FAN, RenderData.VertexIndex.data(), RenderData.VertexCount.data(), RenderData.VertexIndex.size()));

	GL_CALL(glBindVertexArray(0));
	MapShader.StopUsing();
}

void MRenderer::RenderBackMapLayers() const
{
	const BTexture& TilesetTexture = ResourceStore.RequestTexture(SceneSwitcher.GetCurrentMap()->Tileset.Name);
	static const auto& MapShader = ResourceStore.RequestShaderProgram(ResourceStore.Keys.SHADER_PROG_MAP);

	auto& RenderData = RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_MAP_BACK);

	MapShader.Use();
	GL_CALL(glBindVertexArray(RenderData.VAO));

	GL_CALL(glBindTexture(GL_TEXTURE_2D, TilesetTexture));

	GL_CALL(glMultiDrawArrays(GL_TRIANGLE_FAN, RenderData.VertexIndex.data(), RenderData.VertexCount.data(), RenderData.VertexIndex.size()));

	GL_CALL(glBindVertexArray(0));
	MapShader.StopUsing();
}

void MRenderer::RenderActors() const
{
	static const auto& ActorShader = ResourceStore.RequestShaderProgram(ResourceStore.Keys.SHADER_PROG_ACTOR);
	ActorShader.Use();

	static const GLint ModelMatrixLoc = glGetUniformLocation(ActorShader, "model_mat");
	static const GLint TexOrientLoc = glGetUniformLocation(ActorShader, "tex_orientation");

	for (const auto Actor : ActorHandler.GetAllActors())
	{
		if (Actor->IsEnabled() && (Actor->GetActorType() != EActorType::Audio))
		{
			auto AnimTexture = Actor->GetCurrentTexture();
			const auto& RenderData = ActorRenderData.at(Actor->GetID());

			GL_CALL(glBindVertexArray(RenderData.VAO));
			GL_CALL(glBindTexture(GL_TEXTURE_2D, AnimTexture));

			glm::mat4 ModelMatrix = RenderData.ModelMatrix.at(0);
			glm::vec2 TextureOrientation = Actor->GetOrientation();

			GL_CALL(glUniformMatrix4fv(ModelMatrixLoc, 1, GL_FALSE, glm::value_ptr(ModelMatrix)));
			GL_CALL(glUniform2fv(TexOrientLoc, 1, &TextureOrientation[0]));

			GL_CALL(glDrawArrays(GL_TRIANGLE_FAN, 0, RenderData.VertexCount.at(0)));
		}
	}

	ActorShader.StopUsing();
}

void MRenderer::RenderSAP() const
{
	static const auto& SAPRenderMap = RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_SAP);
	static const auto& ShapeShader = ResourceStore.RequestShaderProgram(ResourceStore.Keys.SHADER_PROG_SHAPE);
	
	ShapeShader.Use();

	static const GLuint VertLoc = ShapeShader.GetAttribLoc("vert");
	static const GLuint AlphaLoc = ShapeShader.GetAttribLoc("alpha");
	static const GLuint ColorLoc = ShapeShader.GetUniformLoc("color");
	static const GLuint ModelMatrixLoc = ShapeShader.GetUniformLoc("model_mat");
	static const std::array<GLfloat, 3> Color = { 1.0f, 0.0f, 0.0f };

	GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_SAP).VBO));

	GL_CALL(glEnableVertexAttribArray(VertLoc));
	GL_CALL(glVertexAttribPointer(VertLoc, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, nullptr));

	GL_CALL(glEnableVertexAttribArray(AlphaLoc));
	GL_CALL(glVertexAttribPointer(AlphaLoc, 1, GL_FLOAT, GL_TRUE, sizeof(GLfloat) * 3, (const GLvoid*)(2 * sizeof(GLfloat))));

	GL_CALL(glUniform3fv(ColorLoc, 1, Color.data()));

	for (int i = 0; i < SAPRenderMap.VertexCount.size(); i++)
	{
		const auto& ModelMatrix = SAPRenderMap.ModelMatrix.at(i);
		GLsizei Index = SAPRenderMap.VertexIndex.at(i);
		GLsizei Count = SAPRenderMap.VertexCount.at(i);

		GL_CALL(glUniformMatrix4fv(ModelMatrixLoc, 1, GL_FALSE, glm::value_ptr(ModelMatrix)));
		GL_CALL(glDrawArrays(GL_LINE_LOOP, Index, Count));
	}

	GL_CALL(glDisableVertexAttribArray(0));
	ShapeShader.StopUsing();
}

void MRenderer::RenderBackParallax() const
{
	static const auto& ParallaxShader = ResourceStore.RequestShaderProgram(ResourceStore.Keys.SHADER_PROG_PARALLAX);
	
	ParallaxShader.Use();

	static const GLint ModelMatrixLoc = glGetUniformLocation(ParallaxShader, "model_mat");

	for (const auto& KVP : ParallaxStore.GetCurrentGroup())
	{
		const auto& Parallax = KVP.second;

		if (Parallax.bIsEnabled && (!Parallax.bRenderFront))
		{
			const std::string Key = KVP.first;
			const auto& RenderData = ParallaxRenderData.at(Key);

			GL_CALL(glBindVertexArray(RenderData.VAO));
			GL_CALL(glBindTexture(GL_TEXTURE_2D, ResourceStore.RequestTexture(Key)));

			glm::mat4 ModelMatrix = RenderData.ModelMatrix.at(0);
			GL_CALL(glUniformMatrix4fv(ModelMatrixLoc, 1, GL_FALSE, glm::value_ptr(ModelMatrix)));

			GL_CALL(glDrawArraysInstanced(GL_TRIANGLE_FAN, 0, RenderData.VertexCount.at(0), RenderData.Instances));
		}
	}

	ParallaxShader.StopUsing();
}

void MRenderer::RenderFrontParallax() const
{
	static const auto& ParallaxShader = ResourceStore.RequestShaderProgram(ResourceStore.Keys.SHADER_PROG_PARALLAX);

	ParallaxShader.Use();

	static const GLint ModelMatrixLoc = glGetUniformLocation(ParallaxShader, "model_mat");

	for (const auto& KVP : ParallaxStore.GetCurrentGroup())
	{
		const auto& Parallax = KVP.second;

		if (Parallax.bIsEnabled && Parallax.bRenderFront)
		{
			const std::string Key = KVP.first;
			const auto& RenderData = ParallaxRenderData.at(Key);

			GL_CALL(glBindVertexArray(RenderData.VAO));
			GL_CALL(glBindTexture(GL_TEXTURE_2D, ResourceStore.RequestTexture(Key)));

			glm::mat4 ModelMatrix = RenderData.ModelMatrix.at(0);
			GL_CALL(glUniformMatrix4fv(ModelMatrixLoc, 1, GL_FALSE, glm::value_ptr(ModelMatrix)));

			GL_CALL(glDrawArraysInstanced(GL_TRIANGLE_FAN, 0, RenderData.VertexCount.at(0), RenderData.Instances));
		}
	}

	ParallaxShader.StopUsing();
}

void MRenderer::RenderColliders()
{
	static const auto& ShapeShader = ResourceStore.RequestShaderProgram(ResourceStore.Keys.SHADER_PROG_SHAPE);

	const auto& StaticRenderMap = RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_STATIC_COLLIDER);
	const auto& DynamicRenderMap = RenderDataMap.at(ResourceStore.Keys.RENDER_DATA_DYNAMIC_COLLIDER);

	ShapeShader.Use();

	static const GLuint VertLoc = ShapeShader.GetAttribLoc("vert");
	static const GLuint AlphaLoc = ShapeShader.GetAttribLoc("alpha");
	static const GLuint ColorLoc = ShapeShader.GetUniformLoc("color");
	static const GLuint ModelMatrixLoc = ShapeShader.GetUniformLoc("model_mat");
	static const std::array<GLfloat, 3> Color = { 0.0f, 1.0f, 0.0f };

	GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, StaticRenderMap.VBO));

	GL_CALL(glEnableVertexAttribArray(VertLoc));
	GL_CALL(glVertexAttribPointer(VertLoc, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, nullptr));

	GL_CALL(glEnableVertexAttribArray(AlphaLoc));
	GL_CALL(glVertexAttribPointer(AlphaLoc, 1, GL_FLOAT, GL_TRUE, sizeof(GLfloat) * 3, (const GLvoid*)(2 * sizeof(GLfloat))));

	GL_CALL(glUniform3fv(ColorLoc, 1, Color.data()));
	
	for (int i = 0; i < StaticRenderMap.VertexCount.size(); i++)
	{
		const auto& ModelMatrix = StaticRenderMap.ModelMatrix.at(i);
		GLsizei Index = StaticRenderMap.VertexIndex.at(i);
		GLsizei Count = StaticRenderMap.VertexCount.at(i);

		GL_CALL(glUniformMatrix4fv(ModelMatrixLoc, 1, GL_FALSE, glm::value_ptr(ModelMatrix)));
		GL_CALL(glDrawArrays(GL_LINE_LOOP, Index, Count));
	}

	GL_CALL(glBindBuffer(GL_ARRAY_BUFFER, DynamicRenderMap.VBO));

	GL_CALL(glEnableVertexAttribArray(VertLoc));
	GL_CALL(glVertexAttribPointer(VertLoc, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, nullptr));

	GL_CALL(glEnableVertexAttribArray(AlphaLoc));
	GL_CALL(glVertexAttribPointer(AlphaLoc, 1, GL_FLOAT, GL_TRUE, sizeof(GLfloat) * 3, (const GLvoid*)(2 * sizeof(GLfloat))));

	GL_CALL(glUniform3fv(ColorLoc, 1, Color.data()));

	for (int i = 0; i < DynamicRenderMap.VertexCount.size(); i++)
	{
		const auto& ModelMatrix = DynamicRenderMap.ModelMatrix.at(i);
		GLsizei Index = DynamicRenderMap.VertexIndex.at(i);
		GLsizei Count = DynamicRenderMap.VertexCount.at(i);

		GL_CALL(glUniformMatrix4fv(ModelMatrixLoc, 1, GL_FALSE, glm::value_ptr(ModelMatrix)));
		GL_CALL(glDrawArrays(GL_LINE_LOOP, Index, Count));
	}

	ShapeShader.StopUsing();
}
