#include "BMixChunk.hpp"

BMixChunk::BMixChunk()
	: Chunk(nullptr, nullptr)
{}

BMixChunk::BMixChunk(ChunkPtr Chunk_)
	: Chunk(nullptr, nullptr)
{
	Chunk = std::move(Chunk_);
}

BMixChunk::BMixChunk(BMixChunk&& Other)
	: Chunk(nullptr, nullptr)
{
	if (this != &Other)
	{
		Chunk = std::move(Other.Chunk);
	}
}

BMixChunk& BMixChunk::operator=(BMixChunk&& Other)
{
	if (this != &Other)
	{
		Chunk = std::move(Other.Chunk);
	}

	return *this;
}

BMixChunk::operator Mix_Chunk *()
{
	return Chunk.get();
}
