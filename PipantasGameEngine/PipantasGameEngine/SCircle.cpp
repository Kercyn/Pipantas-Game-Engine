#include "SCircle.hpp"
#include "Utils.hpp"
#include "SDL_stdinc.h"
#include "glm\gtx\projection.hpp"
#include "glm\gtx\transform.hpp"
#include "Managers.hpp"

#include <cmath>

SCircle::SCircle(glm::vec2 Center_, float Radius_)
	: Radius(Radius_), Rotation(0.0f)
{
	Position = Center_;
	Type = EShapeType::Circle;
}

SCircle::SCircle()
	: SCircle(glm::vec2(), 0.0f)
{
}

std::unique_ptr<SShape> SCircle::Clone() const
{
	return std::make_unique<SCircle>(*this);
}

Line SCircle::ProjectInto(const glm::vec2& Axis) const
{
	Line Projection;
	Projection.a = glm::proj(Position + (Radius * Axis), Axis);
	Projection.b = glm::proj(Position - (Radius * Axis), Axis);

	return Projection;
}


bool SCircle::IsPointInside(const glm::vec2& Point) const
{
	return glm::length(Position - Point) < Radius;
}

glm::mat4 SCircle::GetModelMatrix() const
{
	return TranslationMat * glm::mat4(RotationMat) * glm::mat4(ScaleMat);
}

glm::vec2 SCircle::GetSize() const
{
	return glm::vec2(Radius * 2, Radius * 2);
}

void SCircle::SetRadius(float NewRadius)
{
	Radius = NewRadius;
	UpdateVerticesAndNormals();
}

float SCircle::GetRadius() const
{
	return Radius;
}

void SCircle::Rotate(float Radians)
{
	Rotation += Radians;

	while (Rotation > (2 * Utils::PI))
	{
		Rotation -= (2 * Utils::PI);
	}

	UpdateRotationMat();
}

void SCircle::SetRotation(float Radians)
{
	Rotation = Radians;

	while (Rotation > (2 * Utils::PI))
	{
		Rotation -= (2 * Utils::PI);
	}

	UpdateRotationMat();
}

void SCircle::UpdateVerticesAndNormals()
{
	static constexpr float AngleStep = (2 * Utils::PI) / SCircle::CircleVertices;
	float Angle = 0.0f;

	Vertices.clear();

	for (std::size_t i = 0; i < SCircle::CircleVertices; i++)
	{
		Vertices.emplace_back(glm::vec2(std::cos(Angle) * Radius, std::sin(Angle) * Radius));
		Normals.emplace_back(glm::vec2(std::cos(Angle), std::sin(Angle)));
		Angle += AngleStep;
	}
}

void SCircle::UpdateRotationMat()
{
	RotationMat = glm::mat2(glm::rotate(glm::mat4(1.0f), Rotation, glm::vec3(0.0f, 0.0f, 1.0f)));
}
