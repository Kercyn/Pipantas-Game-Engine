#version 330 core

layout (std140) uniform shared_data
{
	mat4 camera;
};

in vec2 vert;
in vec2 vertTexCoord;
in float alpha;

out vec2 fragTexCoord;
out float fragAlpha;

void main()
{
    fragTexCoord = vertTexCoord;
    fragAlpha = alpha;
    gl_Position = camera * vec4(vert, 0.0, 1.0);
}
