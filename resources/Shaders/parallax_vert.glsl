#version 330 core

layout (std140) uniform shared_data
{
	mat4 camera;
};

uniform mat4 model_mat;

in vec3 vert;
in vec2 vertTexCoord;
in vec2 offset;

out vec2 fragTexCoord;

void main()
{
    gl_Position = camera * model_mat * vec4(vert.x + offset.x, vert.y + offset.y, 0.0, 1.0);

	fragTexCoord = vertTexCoord;
}
