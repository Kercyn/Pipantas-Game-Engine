#version 330 core

uniform sampler2D sampler;

in vec2 fragTexCoord;
in float fragAlpha;

out vec4 finalColor;

void main()
{
	vec4 tex = texture2D ( sampler, fragTexCoord );
	gl_FragColor = vec4(tex.r, tex.g, tex.b, tex.a * fragAlpha);
}
