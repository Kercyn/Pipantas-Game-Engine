#version 330 core

layout (std140) uniform shared_data
{
	mat4 camera;
};

uniform mat4 model_mat;

in vec3 vert;
in float alpha;

out float frag_alpha;

void main()
{
	frag_alpha = alpha;
    gl_Position = camera * model_mat * vec4(vert, 1);
}