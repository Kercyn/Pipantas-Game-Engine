#version 330

uniform vec3 color;

in float frag_alpha;

out vec4 out_color;

void main()
{
    out_color = vec4(color, frag_alpha);
}