ControllableChar = {}

local CurrJumpSpeed = {}
local LaunchPos = {}

function test(CharID)
	local Char = PCharacter.new(CharID)
	local State = Char:GetProperty("State")
	local x, y = Char:GetPosition()
	print(tostring(x) .. ", " .. tostring(y))
	print(State)
end

function ControllableChar.MoveLeft(CharID)
	local Char = PCharacter.new(CharID)

	local Speed = tonumber(Char:GetProperty("Speed"))
	
	Char:Translate(-Speed, 0.0)
	Char:SetOrientation(-1.0, 1.0)
	
	if Char:HasCollider("Attack") then
		local AttackCollider = PCollider.new(Char:GetColliderID("Attack"))
		local AttOffsetX, AttOffsetY = AttackCollider:GetOffset()
		if AttOffsetX > 0 then
			AttackCollider:SetOffset(-AttOffsetX, AttOffsetY)
		end

		local State = Player:GetProperty("State")
		local IsNotAttacking = (State ~= "attacking") and (State ~= "jump_attacking") and (State ~= "fall_attacking")
		if Char:GetProperty("IsGrounded") == "true" and IsNotAttacking and State ~= "jumping" then
			Char:SetProperty("State", "running")
		end
	end
end

function ControllableChar.MoveRight(CharID)
	local Char = PCharacter.new(CharID)

	local Speed = tonumber(Char:GetProperty("Speed"))
	
	Char:Translate(Speed, 0.0)
	Char:SetOrientation(1.0, 1.0)
	
	if Char:HasCollider("Attack") then
		local AttackCollider = PCollider.new(Char:GetColliderID("Attack"))
		local AttOffsetX, AttOffsetY = AttackCollider:GetOffset()
		if AttOffsetX < 0 then
			AttackCollider:SetOffset(-AttOffsetX, AttOffsetY)
		end

		local State = Player:GetProperty("State")
		local IsNotAttacking = (State ~= "attacking") and (State ~= "jump_attacking") and (State ~= "fall_attacking")
		if Char:GetProperty("IsGrounded") == "true" and IsNotAttacking and State ~= "jumping" then
			Char:SetProperty("State", "running")
		end
	end
end

function ControllableChar.StartAttacking(CharID)
	local Char = PCharacter.new(CharID)
	local State = Char:GetProperty("State")

	if Char:GetProperty("CanAttack") == "true" then
		if State == "falling" then
			Player:SetProperty("State", "fall_attacking")
		elseif State == "jumping" then
			Player:SetProperty("State", "jump_attacking")
		else
			Player:SetProperty("State", "attacking")
		end

		CharacterAttackColliders[CharID]:Enable()		
		Char:SetProperty("CanAttack", "false")
	end
end

function ControllableChar.StopAttacking(CharID)
	local Char = PCharacter.new(CharID)
	local State = Char:GetProperty("State")

	if State == "fall_attacking" then
		Char:SetProperty("State", "falling")
	elseif State == "jump_attacking" then
		Char:SetProperty("State", "jumping")
	else
		Char:SetProperty("State", "stopped_attacking")
	end
	
	CharacterAttackColliders[CharID]:Disable()
	Char:SetProperty("CanAttack", "true")

end

function ControllableChar.StartJumping(CharID)
	local Char = PCharacter.new(CharID)
	local _, CharY = Char:GetPosition()
	local State = Char:GetProperty("State")

	if Char:GetProperty("IsGrounded") == "true" then
		Char:SetProperty("CanJump", "true")
		
		local IsAttacking = (State == "attacking") or (State == "jump_attacking") or (State == "fall_attacking")
		if IsAttacking then
			Char:SetProperty("State", "jump_attacking")
		else
			Char:SetProperty("State", "jumping")
		end
		
		LaunchPos[CharID] = CharY
	end
end

function ControllableChar.StopJumping(CharID)
	local Char = PCharacter.new(CharID)
	local State = Char:GetProperty("State")
	
	if State == "jump_attacking" or State == "fall_attacking" then
		Char:SetProperty("State", "attacking")
	else
		Char:SetProperty("State", "stopped_jumping")
	end
	
	Char:SetProperty("CanJump", "false")
end

function ControllableChar.Jump(CharID)
	local Char = PCharacter.new(CharID)
	local State = Char:GetProperty("State")

	
	local Char = PCharacter.new(CharID)
	local CharX, CharY = Char:GetPosition()
	local _, ColliderHeight = CharacterBodies[CharID]:GetSize()
	
	local HasHitTop = Pipantas:Raycast(CharX, CharY, 0.0, 1.0, (ColliderHeight / 2) + 1.0, "00000000001", false)
	local HasReachedMaxHeight = (CharY - LaunchPos[CharID]) >= tonumber(Char:GetProperty("JumpHeight"))
	
	if HasHitTop or HasReachedMaxHeight then
		ControllableChar.StopJumping(CharID)
	end
	
	if Char:GetProperty("CanJump") == "true" then
		local CurrJumpSpeed = tonumber(Char:GetProperty("JumpSpeed"))

		Char:Translate(0, CurrJumpSpeed)
		if CurrJumpSpeed > 1000 then
			CurrJumpSpeed = CurrJumpSpeed - 100
		end
	end
end
