function KillTrap_OnTriggerEnter(ThisID, OtherID)
	local ActorID = PCollider.new(OtherID):GetAttachedActorID()
	KillCharacter(ActorID)
end

function KillTrap_OnTriggerExit(ThisID, OtherID)
end

function KillTrap_OnTriggerStay(ThisID, OtherID)
end
