function Checkpoint_OnTriggerEnter(ThisID, OtherID)
	local ThisCollider =  PTriggerCollider.new(ThisID)
	
	local Number = tonumber(ThisCollider:GetProperty("Number"))
	local x = tonumber(ThisCollider:GetProperty("RespawnX"))
	local y = tonumber(ThisCollider:GetProperty("RespawnY"))
	
	UpdateCurrentCheckpoint(Number, x, y)
end

function Checkpoint_OnTriggerExit(ThisID, OtherID)
end

function Checkpoint_OnTriggerStay(ThisID, OtherID)
end
