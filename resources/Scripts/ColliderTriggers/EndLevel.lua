function EndLevel_OnTriggerEnter(ThisID, OtherID)
	local SceneID = PTriggerCollider.new(ThisID):GetProperty("NextLevel")
	Pipantas:ChangeScene(SceneID)
end

function EndLevel_OnTriggerExit(ThisID, OtherID)
end

function EndLevel_OnTriggerStay(ThisID, OtherID)
end
