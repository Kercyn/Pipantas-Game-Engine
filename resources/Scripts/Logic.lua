--EPSILON = 1.1920928955078125e-07

local bAreControlsEnabled = true

function KillCharacter(CharID)
	local Char = PActor.new(CharID)
	
	if Char:GetProperty("State") ~= "dead" then
		Char:SetProperty("CurrHP", 0)
		Char:SetProperty("State", "dead")
		DisableControls()
	end
end

function RespawnPlayer()
	local x, y = GetCurrentCheckpointPos()
	Player:SetPosition(x, y)
	Player:SetProperty("CurrHP", Player:GetProperty("MaxHP"))
	Player:SetProperty("State", "idle")
	EnableControls()
end

function DisableControls()
	bAreControlsEnabled = false
end

function EnableControls()
	bAreControlsEnabled = true
end

function AreControlsEnabled()
	return bAreControlsEnabled
end

function CameraUpdate()
	local x, y = Player:GetPosition()

	Camera:SetPosition(x, y + 200.0)
end